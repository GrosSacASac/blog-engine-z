import child_process from "node:child_process";
import url from "node:url";
import fs from "node:fs"
import { dirname, normalize } from "node:path";

import {copyDirectory} from "filesac";
import { buildSite } from "../../source/buildSite.js";
import { defaultBuildOptions } from "../../defaultSettings/defaultBuildOptions.js";


const __filename = url.fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const BIG_FILE_SIZE = 10**2;
const templateFolder = `${__dirname}/../blog-example`;
const inputFolder = `${__dirname}/blog-example-copy/`;



console.warn("I hope you have at least 100GB of space")

const totalSteps = 3
// delete directory recursively
fs.rmSync(inputFolder, { recursive: true, force: true });
console.log(`step 0/${totalSteps} clean old files`)

const u8Text = new TextEncoder().encode(`Hi there
`)
copyDirectory(templateFolder, inputFolder).then(function () {
    console.log(`step 1/${totalSteps} copied file structure from ${normalize(templateFolder)}`)
    child_process.exec(`python.exe ${__dirname}/__main__.py`, {shell:"pwsh.exe"}, async function (error, out, outerr) {
        
        if (error) {
            console.error({
                error, out, outerr
            })
            return
        }
        
        let writes = 0;
        const veryLargeFile = fs.createWriteStream(`${inputFolder}source/bigfile.txt`);
        let shouldContinue;
        const totalWritesWanted =  BIG_FILE_SIZE / u8Text.length;
        while (writes < totalWritesWanted) {
            writes += 1;
            shouldContinue = veryLargeFile.write(u8Text);
            if (!shouldContinue) {
                await (new Promise((resolve, reject) => {
                    veryLargeFile.once("drain", () => {
                        shouldContinue = true;
                        resolve();
                    });
                }))
            }
        }
        veryLargeFile.end(() => {
            console.log(`step 2/${totalSteps} generated a large amount of input files`)
            
            console.log(`start measuring time ...`)
            console.time("time")
            
            // same as: node ./source/main.js inputFolder=./tests/performance/blog-example
            buildSite({
                ...defaultBuildOptions,
                inputFolder,
                outputFolder: `${__dirname}/result/`,
            }).then(function () {
                console.log(`step 3/${totalSteps} ran blog-engine-sac, ${`${__dirname}/result/`} is ready`)
                console.timeEnd("time")
            }).catch(function(error) {
                console.timeEnd("time")
                console.log(`step 3/${totalSteps} error blog-engine-sac`)
                console.error(error)
            });
        
        });
    })
})

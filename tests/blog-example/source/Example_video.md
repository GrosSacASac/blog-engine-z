
## Video


![bbb_sunflower](../videos/bbb_sunflower_1080p_30fps_normal.mp4)

If absent, download https://download.blender.org/demo/movies/BBB/bbb_sunflower_1080p_30fps_normal.mp4 and put it inside videos folder

## Audio

![zen music](../audio/wav.wav)

If absent, download create a wav file and put it inside audio folder with the name wav.wav


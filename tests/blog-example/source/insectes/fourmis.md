# Les fourmis

Les fourmis sont parmi les insectes ayant colonisé tous les lieux fertiles de la terre. Comme les humains, ils sont capables de façonner des galeries et sont adeptes de l'agriculture.

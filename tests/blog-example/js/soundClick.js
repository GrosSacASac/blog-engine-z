const audioTarget = document.createElement("audio");
audioTarget.src= "../audio/wav.wav";
document.body.appendChild(audioTarget)
document.body.addEventListener("click", function() {
    const clone = audioTarget.cloneNode();
    document.body.appendChild(clone);
    clone.fastSeek(61.6); // in s
    clone.play();
    setTimeout(() => {
        clone.pause();
        clone.remove();
    }, 580)
});

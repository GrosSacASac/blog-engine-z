// todo deduplicate from tests/performance
import url from "node:url";
import fs from "node:fs"
import path from "node:path";

import { buildSite } from "../../source/buildSite.js";
import { defaultBuildOptions } from "../../defaultSettings/defaultBuildOptions.js";


const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const templateFolder = `${__dirname}/../blog-example/`;
const outputFolder = `${__dirname}/result/`;




const totalSteps = 1;
// delete directory recursively
fs.rmSync(outputFolder, { recursive: true, force: true });
console.log(`step 0/${totalSteps} clean old files`);


   
console.log(`start measuring time ...`);
console.time("time");

// same as: node ./source/main.js inputFolder=./tests/performance/blog-example
buildSite({
    ...defaultBuildOptions,
    inputFolder: templateFolder,
    outputFolder,
}).then(function () {
    console.timeEnd("time");
    console.log(`step 1/${totalSteps} ran blog-engine-sac, ${outputFolder} is ready`);

}).catch(function(error) {
    console.timeEnd("time");
    console.log(`step 1/${totalSteps} error blog-engine-sac`);
    console.error(error);

});

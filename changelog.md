# changelog

## 11.3.0

* Add RSS, JSON and Atom feeds

## 11.2.0

* Handle option homePageImage, default is home.jpg

## 11.0.0

* Add ability to organize posts into folders inside source/. If that is done, categories are automatically created based on that folder name.
* The extras folder is now supposed to be at the base. Move /source/extras to /extras. in other words: Next to source/, but not inside.
* Minimum Node version is 18

## 10.10.0

* Improve performance when using many images
* Allow media to use subfolders
* scaleImage cli now takes path to in an out image folder directly and does not assume output to be results anymore

## 10.9.0

* Add js option for individual posts

## 10.8.0

* Add allowedAccess option, default is / (all), only media from this path can be referenced

## 10.7.0

* All media can be outside the blog folder and be referenced with a relative link

## 10.6.0

* Add lb translations
* Add pt translations
* Add de translations

## 10.5.0

* Add api option publicLogger
* Add api option deletePreviousOutput
* Add api option resizeImages
* Add api option fetchRemoteMedia

## 10.4.0

* Add support for secret url (pro)

## 10.3.0

* Use best resolution in slides
* Use lazy loading for images (after 4 images)

## 10.2.0

* Add support for slide shows (pro)

## 10.1.0

* Add table support

## 10.0.0

### More intuitive categories

* posts "tags" option renamed to "categories"
* posts "categories" option renamed to "tags"
* more consistent with blog settings
* improve search engines with "tags" provided

### Split blog-example

* into blog-template, which should be used as a starter
* and into tests/blog-example, which uses every feature possible and is used by automated tests
* the blog-template no longer uses every feature possible and thus is less confusing as a starter

## 9.2.0

* add outputFolder cli option

## 9.1.0

* better markdown parsing
* better svg support

## 9.0.0

* Minimum Node version is 16
* Minimum npm version is 8

## 8.6.0

* author option can be an URL
* better console output

## 8.5.0

* coverImage option can be a an absolute url to an image.

## 8.2.0

* Add coverImage option, and the possibility to disable it.

## 8.1.0

* Add listed option

## 8.O.0

Breaking changes:

about.md and contact.md need to be inside the same folder as other article sources now. Move them from source/extras/ into source/.
It also means that they have all the same benefits as regular posts (comments, translations, non markdown formats etc).

Translated posts should no longer have duplicated lang in the filename.

Posts are now properly sorted by creation date.

## 7.13.0

* Support every image type

## 7.12.0

* Receive webmention
* add commentsEnabled option to force comments on non-indexed pages

## 7.11.0

* Print styles
* Correct machine readable dates
* Relative images inside blog post use dynamic sizing
* fix: do not upscale image if smaller than standard width
* Opengraph support, add url option
* Support audio and video inside posts
* fetch remote media while building the blog

## 7.10.0

Handle infinitely large md and txt files

## 7.9.0

Stop indexing pure navigation pages

## 7.8.0

Add syntax highlighting

## 7.7.0

Add option standalone for posts

## 7.6.0

Use canonicalUrl: false on a post to disable canonical link

## 7.5.0

Add possibility to translate category names

## 7.4.0

Allow translation sources to be any URL

## 7.3.0

Do not let search engines index non indexed pages

## 7.2.0

Display the categories posts number. Hide empty categories

## 7.1.0

Each category is available for each language

## 7.0.0

Local file paths are relative to the source directory

Ability to force canonical url

Handle remote content cache hits

Do not create a non indexed category page for each word of each title

Comment rules can be different for each language

## 6.8.0

Before, only 4000 posts could be handled, now over 12000 (have not tested more)

## 6.7.0

Disable comments for non indexed pages

## 6.6.0

Enable comment rules url

## 6.5.3

Free textarea resize, bring back node 14 compatibility

## 6.5.0

Comments can be disabled on a per post basis

## 6.4.0

Ask for localized comment system

## 6.3.0

If translation is not found, fall back to non regional translation

## 6.2.0

Start Pro version, add comment system in pro version

## 6.1.0

Add option indexInBaseIndexLink. If false the link for index becomes /. If true it becomes /index.html or /index. Default is false

## 6.0.1

Translate dates

## 6.0.0

Previous result/ folder is deleted before each npm run sac command.

scaleImages runs inside sac command. It is not necessary to run the command separately.

## 5.21.0

all images are resized

## 5.20.0

add canonical link for remote content and canonicalUrl post option to force an url

## 5.19.0

add editLink option for post

## 5.18.0

inputFolder cli option now usable with scaleImages command

## 5.17.1

Actually useful error message if source is not found

## 5.17.0

Add htmlExtensionLinks option. If true, links will have the ".html" extension. Default is false.

## 5.16.0

Launch blog engine from any location with
`inputFolder=./path/to/my-site` cli option. Default is `inputFolder=.`

## 5.15.0

Dark Mode

## 5.14.1

More accessible

## 5.14.0

Add links to translated indexes

## 5.13.1

Document all the features in the readme

## 5.12.4

Result files are minified

## 5.12.3

Better contrast on menu links

## 5.10.0

Start a changelog

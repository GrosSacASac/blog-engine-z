# [Blog-engine-sac](https://gitlab.com/GrosSacASac/blog-engine-z)

Free and open static site generator, that reads your input files and outputs web pages. [My blog as an example](https://letz.social/blog/index-en-GB)

## Feature Overview

### Friendly

- Support audio and video inside posts
- Post sources can be text, markdown, html and more
- Post sources can be any valid URL or local file
- Semi private posts with indexed: false
- Translation ready

### Modern

- Responsive design and adaptive images
- 100% lighthouse score, WCAG 2.1 compliant
- Cleaner than 94 % of web pages tested on websitecarbon.com
- Output files are minified
- Dark mode ready
- Works without JavaScript
- Fast (250 files in less than 3 seconds) and cli based

### SEO friendly

- Index page for all posts and categories
- Client-side, ultra fast search
- RSS, JSON and Atom feeds
- Opengraph support

## Pro version

To support the project and have additional features consider the pro version. [Open an issue](https://gitlab.com/GrosSacASac/blog-engine-z/-/issues/new) or [chat](https://miaou.dystroy.org/3589) if interested. The pro version includes

- Comment system
- Slide shows
- Update blog via simple user interface
- Partial Webmention support
- Web hosting and domain setup, costs included
- Help to set up and/or help to migrate from other CMS
- 6 hours of custom feature development for the site or the engine
- Issues have priority over free users' issues

**Cost**: 650€ per site a year

## How to use ?

### Set up

1. [Download NodeJS 16+](https://nodejs.org/en/)

#### Set up with the template

2. [Copy template](https://gitlab.com/GrosSacASac/blog-engine-z/-/tree/master/blog-template) and follow instructions in its readme

#### Set up manually

2. Create a new project (folder)
3. Create a valid package.json file with `npm init` for example
4. Run [`npm i blog-engine-sac`](https://www.npmjs.com/package/blog-engine-sac) (may require sudo and --unsafe-perm=true --allow-root)
5. add scripts  inside package.json

```json
  "scripts": {
    "sac": "node node_modules/blog-engine-sac/source/main.js inputFolder=."
  },
```

6. follow instructions below to create required files
7. `npm run sac`
8. to view the results use a static file server

### Create a new post

Create a new file in the folder `source/`. Supported file types:

- text (.txt)
- markdown (.md)
- html (.html)
- yaml (.yaml)
- json (.json)
- ini (.ini)
- toml (.toml)

A markdown file is a normal text file with `.md` file extension. The name of the file is used as the name of the post.

#### Simple post

Create a file in `source/`. For example my-post.md or my-post.txt.

#### Post with metadata

Create a simple post then create a meta file with the same base name. For example my-post.yaml. Meta files in the readme will be yaml, but they can be any format in the list:

- yaml
- json
- ini
- toml

```yaml
coverImage: "post.jpg"
# set coverImage to false to disable it
title: "I am about to write a title"
description: "Description"
lang: en
author: "Author name or URL"
ready: true
indexed: true
listed: true
editLink: false
standalone: false
creationDate: 2018/02/15
modifiedDate: 2035/02/19
categories:
  - cooking
  - sports
  - travel
tags:
  - fun
  - "874569111fe8"
translations: []
js: []
license: CC0-1.0
# comments
# commentsDisabled: false
```

##### ready

set ready to false to not process them at all.

##### indexed

set indexed to false, to have it in the blog, but the index.html will not have a link to it and search will not find it. The only way to open it would be to know the exact URL.

##### listed

set listed to false, to have it in the blog, but the index.html will not have a link to it. Unlisted posts can still be found with search or by knowing the URL. If indexed is false, then the listed option has no effect.

##### categories (of the blog post)

categories is a list of categories that your post will appear in. Categories are pages that list only post of that category. For example if you have "travel" inside categories inside blog-engine-sac settings, and travel as a category of the post, then the post will be listed on the travel category. Categories that are not in the blog settings categories will create a semi hidden category : a link from post to that category but unlisted on the index page.

##### tags

tags is a list of keywords specifically to aid people using those keywords to search for the post and find it. They are hidden to humans and visible to search engines

##### translations

Use it to mark posts as translations of another post. If we have the following files:

- `Geography.md` (in English)
- `Géographie.md` (in French)
- `Erdkunde.md` (in German)

Use the following meta file to link them:

###### `Geography.yaml`

```yaml
lang: en
translations:
  - lang: fr
    src: ./Géographie.md
  - lang: de
    src: ./Erdkunde.md
```

Each translation item may use any key value pair seen above (title, author, etc)

##### editLink

Should be a url or false. If it is an url, an edit link will be created for that post

##### js

One or multiple js files to be included with that page

##### standalone

If true the html output will not be decorated by the usual (header, footer, next, previous, etc)

##### commentsDisabled

If true the comments are disabled

#### canonicalUrl

This should reflect the canonical url for a post. If you are the author of your articles, set this to false. If you take an article from another blog and would like machines to know the original canonical url for the article, set this to the original article's url. By default the canonical url will be self, for local posts, and src for remote posts.

#### External post

Create a file that describes where to find the external post. For example js-style-guide.yaml . Including a post from the web: Add a yaml file in `source/` with `src` set to the url.

##### js-style-guide.yaml

```yaml
src: 'https://raw.githubusercontent.com/GrosSacASac/JavaScript-Set-Up/master/js/red-javascript-style-guide/readme.md'
```

Another  Example with this readme file:

##### blog-engine-sac-readme.yaml

```yaml
src: 'https://gitlab.com/GrosSacASac/blog-engine-z/-/raw/master/README.md'
canonicalUrl: 'https://gitlab.com/GrosSacASac/blog-engine-z/-/blob/master/README.md'
lang: en
Content-Type: text/markdown
```

### site settings

Create file `blog-engine-sac.yaml` with

```yaml
# put the final url of the website and uncomment
# url: https://example.com/
defaultLang: en
defaultAuthor: AUTHOR
mainTitle: Blog
tabTitle: "" # uses mainTitle if left empty
subTitle: Change title and subtitle in blog-engine-sac.yaml
defaultLicense: CC-BY-NC-4.0
htmlExtensionLinks: false
indexInBaseIndexLink: false # index link becomes just /
categories:
  - tag: travel
    "fr": voyages
  - tag: dance
    "fr": danse
    
```

Ideally defaultLicense is a [SPDX identifier](https://spdx.org/licenses/).

defaultLang is a [2 letter shorthand for the language](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) (ISO 639-1).

htmlExtensionLinks: If true, links will have the ".html" extension. Default is false.

Change the values as you see fit.

#### categories (for the entire blog)

categories are provided by the user as an array for the entire blog

for example: travel, cuisine, sports

Each category will have a dedicated index page that acts like the main index page but only lists post of that category. Posts will be in that category if they have a corresponding tag.

### about page

Create file `source/about.md`.

### contact page

Create file `source/contact.md`.

### footer

Create file `source/extras/footer.md`.

### Edit Images

Open folder `images` Replace the images with new ones but keep the exact filename to not break the links. (Images are linked from other files, not copied, the name of the files are used)

### Required images

- `images/home.jpg`
- `images/post.jpg`

### Custom css

Add the following key-value pair in your site settings.

```yaml
customCss: custom.css
```

Then create a folder named `css`. Inside create a file `custom.css`. Then copy inside `custom.css` the first 2 rules [found inside blog-engine-sac.css](https://gitlab.com/GrosSacASac/blog-engine-z/-/blob/master/css/blog-engine-sac.css). These are some css variables easy to edit as a starting point.

### What is mark down

A text format. Learn it here <https://commonmark.org/help/>

## About the code

Don't remove, edit or add any other file unless you know exactly what you are doing!

### HTML files

are generated. Do not edit directly

## About

### Design starting point

[Start Bootstrap - Clean Blog](https://startbootstrap.com/template-overviews/clean-blog/)
is a stylish, responsive blog theme for [Bootstrap](http://getbootstrap.com/) created by [Start Bootstrap](http://startbootstrap.com/). This theme features a blog homepage, about page, contact page, and an example post page along with a working PHP contact form.

### [Changelog](https://gitlab.com/GrosSacASac/blog-engine-z/-/blob/master/changelog.md)

### Copyright and License

[CC0](./license.txt) (Only for blog-engine-sac itself)

# Blog-engine-sac blog-template

This is a template to start a web site using blog engine sac.
Once copied, run (only once)

```
npm i
```


## Commands

 - `npm run sac`
 - `npm run serve`

## Edit files in source

Feel free to remove examples in source/

## Edit blog-engine-sac.yaml

Change the default author name, the blog title and more

## Use your own images

In images/

## How to update ?

```
npm i blog-engine-sac@latest
```

Don't forget to read the changelog https://gitlab.com/GrosSacASac/blog-engine-z/-/blob/master/changelog.md to adapt to breaking changes.


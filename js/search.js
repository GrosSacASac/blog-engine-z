export { searchCodeTemplate };

import {postStartName} from "../html/links.js";
import * as urls from "../defaultSettings/urls.js";


const maxResults = 10;
const searchCodeTemplate = (posts, options) => {
    // todo deduplicate
    const {extensionLinks, defaultLang} = options;
    const tagsAndUrls = posts.map((post) => {
        const { title, searchTags, lang } = post;
        let langPrefix = ``;
        if (lang !== defaultLang) {
            langPrefix = `[${lang}] `;
        }
        return {
            // convert to string for lunr
            searchTags: searchTags.join(` `),
            title: `${langPrefix}${title}`,
            url: `${postStartName(post)}${extensionLinks}`,
        };
    });
    return `
import lunr from "lunr";

const searchResults = document.getElementById("searchResults");
const searchInput = document.getElementById("searchInput");
const searchBlock = document.getElementById("searchBlock");
searchBlock.hidden = false;

const articles = ${JSON.stringify(tagsAndUrls, null, 2)};

const index = lunr(function () {
    this.field("searchTags");
    this.ref("url");

    articles.forEach((item) => {
        this.add(item);
    });
});

const takeRef = (object) => {
    return object.ref;
};

// duplicate from search
const makeUrl = (ref) => {
    // todo refactor
    const topLevel = (!location.pathname.includes("/${urls.posts}") && !location.pathname.includes("/${urls.categories}"))
    let prePath;
    if (topLevel) {
        prePath = \`.\`;
    } else {
        prePath = \`..\`;
    }
    return \`\${prePath}/${urls.posts}\${ref}\`;
};

searchInput.addEventListener("input", (event) => {
    const search = searchInput.value;
    searchResults.innerHTML = "";
    if (search === "") {
        return;
    }
    const rawResults = index.search(event.target.value)
    rawResults.length = Math.min(${maxResults}, rawResults.length); // do not display more
    // rawResults.forEach(console.log);
    const results = rawResults.map(takeRef).map(url => {
        return articles.find(article => {
            return article.url === url;
        });
    });

    const elements = results.map(article => {
        const url = makeUrl(article.url); // root url
        const li = document.createElement("li");
        const a = document.createElement("a");
        a.textContent = article.title;
        a.href = url;
        li.appendChild(a);
        return li;
    });
    elements.reverse();

    const documentFragment = new DocumentFragment();

    elements.forEach(element => {
        documentFragment.appendChild(element);
    });

    
    searchResults.append(documentFragment);
});
`;
};



export { translate, translateUnbound };
import { translations } from "./translations.js";

const translateUnbound = (translations, lang, key) => {
    lang = lang || `en`;
    if (translations[lang]) {
        if (translations[lang][key]) {
            return translations[lang][key];
        }
    }
    // try without region specific
    if (lang.length > 2) {
        return translateUnbound(translations, lang.substr(0,2), key);
    }
    return key;
};

const translate = translateUnbound.bind(undefined, translations);

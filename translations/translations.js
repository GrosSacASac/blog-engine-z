export { translations };

import { en } from "./en.js";
import { fr } from "./fr.js";
import { lb } from "./lb.js";
import { pt } from "./pt.js";
import { de } from "./de.js";

const translations = {
    fr,
    en,
    lb,
    pt,
    de,
};

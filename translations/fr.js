export { fr };


const fr = {
    [`Not Indexed`]: `N'est pas indexé`,
    [`Unlisted`]: `Non répertorié`,
    [`Posts`]: `Articles`,
    [`Categories`]: `Catégories`,
    [`licensed under`]: `Ce contenu est distribué avec la license`,
    [`Unless otherwise stated, the content is licensed under`]: `Sauf mention contraire, le contenu est distribué sous la license`,
    Translations: `Traductions`,
    Home: `Accueil`,
    About: `À propos`,   
    Contact: `Contact`,
    [`Posted by`]: `Posté par`,
    [`Last edit`]: `Dernière modification`,
    [`Search`]: `Chercher`,
    [`Previous`]: `Précedent`,
    [`Next`]: `Suivant`,
    [`Edit`]: `Modifier cette page`,
};

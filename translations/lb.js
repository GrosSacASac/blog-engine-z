export { lb };


const lb = {
    [`Not Indexed`]: `Net indexéiert`,
    [`Unlisted`]: `Net listéiert`,
    [`Posts`]: `Artikeln`,
    [`Categories`]: `Catégorien`,
    [`licensed under`]: `Dese contenu ass lizenséiert enner`,
    [`Unless otherwise stated, the content is licensed under`]: `Ausser exeptioun, ass de conenu enner folgend lizenz`,
    Translations: `Iwwersetzungen`,
    Home: `Start`,
    About: `À propos`,   
    Contact: `Kontakt`,
    [`Posted by`]: `Vun`,
    [`Last edit`]: `Lecht modificatioun`,
    [`Search`]: `Sichen`,
    [`Previous`]: `Vergaangen`,
    [`Next`]: `Follgend`,
    [`Edit`]: `Ännern`,
};

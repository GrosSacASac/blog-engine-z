export { de };


const de = {
    [`Not Indexed`]: `Nicht indiziert`,
    [`Unlisted`]: `Nicht gelisted listéiert`,
    [`Posts`]: `Poste`,
    [`Categories`]: `Kategorien`,
    [`licensed under`]: `Dieser Inhalt ist lizensiert unter`,
    [`Ausser Ausnahme, ist dieser Inhalt lizensiert unter`]: `Ausser exeptioun, ass de conenu enner folgend lizenz`,
    Translations: `Übersetzungen`,
    Home: `Home`,
    About: `À propos`,   
    Contact: `Kontakt`,
    [`Posted by`]: `Geposted von`,
    [`Last edit`]: `Letzte Änderung`,
    [`Search`]: `Suchen`,
    [`Previous`]: `Vorheriges`,
    [`Next`]: `Nächster`,
    [`Edit`]: `Verändern`,
};

export { pt };


const pt = {
    [`Not Indexed`]: `Nao indexado`,
    [`Unlisted`]: `Nao Listado`,
    [`Posts`]: `Posts`,
    [`Categories`]: `Categorias`,
    [`licensed under`]: `Esta contendo esta licencada com`,
    [`Unless otherwise stated, the content is licensed under`]: `Esta contendo esta licencada com`,
    Translations: `Traduçaos`,
    Home: `Accueil`,
    About: `Acerca`,   
    Contact: `Contacto`,
    [`Posted by`]: `Criaodo para`,
    [`Last edit`]: `Ultima mudacao`,
    [`Search`]: `Procurar`,
    [`Previous`]: `Anterior`,
    [`Next`]: `Proximo`,
    [`Edit`]: `Mudar pagina`,
};

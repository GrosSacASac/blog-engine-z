export { makeUrl, linkFromPost, linkFromCategory, escapeHtml, indexFileName, indexLink, categoryFileName, postStartName,
    aboutNameStart,
contactNameStart,
};
// rename because escape is a global function for urls
import {escape as escapeHtml} from 'html-escaper';
import slugify from '@sindresorhus/slugify';
import * as urls from "../defaultSettings/urls.js";
import { translate } from '../translations/translate.js';

// const capitalize = (words) => {
//     return `${words[0].toUpperCase()}${words.substr(1)}`;
// };

const linkFromCategory = (categoryName, {extensionLinks, lang, defaultLang}, subPath = ``) => {
    return `<a href="${subPath}./${urls.categories}${categoryNameStart(categoryName, lang, defaultLang)}${extensionLinks}">${escapeHtml(translate(lang,  categoryName))}</a>`;
};


const postStartName = (post) => {
    // since remote urls translation will have the same base
    let {base2} = post;
    // isTranslation
    if (post.isTranslation && !base2.endsWith(post.lang)) {
        base2 = `${base2}-${post.lang}`;
    }
    if (post.addSecret) {
        base2 = `${base2}_${post.addSecret}`;
    }
    return slugify(base2);
    
};

const linkFromPost = (post, attributes = ``, {extensionLinks}) => {
    return `<a href="./${postStartName(post)}${extensionLinks}" ${attributes}>${escapeHtml(post.title)}</a>`;
};

const makeUrl = (ref) => {
    return `./${urls.posts}${ref}`;
};

const indexFileName = (lang, defaultLang) => {
    let name;
    if (lang && lang !== defaultLang) {
        name = `index-${lang}.html`;
    } else {
        name = `index.html`;
    }
    return name;
};

const categoryNameStart = (categoryName, lang, defaultLang) => {
    let name = slugify(categoryName);
    if (lang && lang !== defaultLang) {
        name = `${name}-${lang}`;
    } else {
        name = `${name}`;
    }
    return name;
};

const aboutNameStart = (lang, defaultLang) => {
    return slugify(categoryNameStart(`about`, lang, defaultLang));
};

const contactNameStart = (lang, defaultLang) => {
    return slugify(categoryNameStart(`contact`, lang, defaultLang));
};

const categoryFileName = (categoryName, lang, defaultLang) => {
    return `${categoryNameStart(categoryName, lang, defaultLang)}.html`;
};

const indexLink = (lang, defaultLang, {extensionLinks, indexInBaseIndexLink}) => {
    let name;
    if (lang && lang !== defaultLang) {
        name = `index-${lang}${extensionLinks}`;
    } else if (indexInBaseIndexLink) {
        name = `index${extensionLinks}`;
    } else {
        name = ``;
    }
    return name;
};

export  { minifyHtml };

import htmlMinifier from "html-minifier";
import { htmlMinifierOptions } from "../defaultSettings/htmlMinifier.js";


const {minify} = htmlMinifier;

const minifyHtml = function(originalHtml) {
    return minify(originalHtml, htmlMinifierOptions);
};

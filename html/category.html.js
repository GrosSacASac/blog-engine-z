export { createCategoryHtml };

import { styleSheets } from "./styles.html.js";
import { createHeaderHtml } from "./header.html.js";
import { createFooterHtml } from "./footer.html.js";
// import { normalizeDate } from "../source/dates.js";
import { htmlListFromPosts } from "./listOfPosts.js";
import { escapeHtml} from "./links.js";
import {makeBackGroundPicture, prepareBackGroundImageSource} from "./images.js";
import { translate } from "../translations/translate.js";
import { minifyHtml } from "./minifyHtml.js";


const createCategoryHtml = (options) => {
    const { posts, categoryName,  defaultAuthor: author, subTitle: description, lang, extensionLinks, homePageImage} = options;
    const escapedCategoryName = escapeHtml(translate(lang,  categoryName));
    const topLevel = false;
    const cover = prepareBackGroundImageSource(homePageImage, {
      dir: options.inputFolder,
      ...options,
    }, topLevel);
    return minifyHtml(`<!doctype html>
<html lang="${lang}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="${description}">
    <meta name="author" content="${author}">
    <meta name="robots" content="noindex">
    <title>${options.tabTitle} - ${escapedCategoryName}</title>
    ${styleSheets(options, topLevel)}
  </head>
  <body>
    ${createHeaderHtml({ ...options, topLevel })}

    <header class="masthead position-relative">
    ${cover}
    <div class="container position-absolute">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10">
            <div class="site-heading">
              <h1>${escapedCategoryName}</h1>
              <h2 class="subheading">${translate(lang, `Posts`)}</h2>
            </div>
          </div>
        </div>
      </div>
    </header>

    <main class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10">
          ${htmlListFromPosts(posts, {lang, topLevel, extensionLinks})}
        </div>
      </div>
    </main>
    ${createFooterHtml({ ...options, topLevel: false })}
  </body>
</html>
`);

};


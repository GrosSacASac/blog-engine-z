export { createIndexHtml };

import { styleSheets } from "./styles.html.js";
import { createHeaderHtml } from "./header.html.js";
import { createFooterHtml } from "./footer.html.js";
import {linkFromCategory, indexLink} from "./links.js";
import { htmlListFromPosts } from "./listOfPosts.js";
import {makeBackGroundPicture, openGraphImage, prepareBackGroundImageSource} from "./images.js";
import { translate } from "../translations/translate.js";
import { minifyHtml } from "./minifyHtml.js";


const createIndexHtml = (options) => {
    const { defaultAuthor: author, subTitle: description, posts, categories, lang, extensionLinks, defaultLang, homePageImage } = options;
    options.medias = [];
    const topLevel = true;
    
    const cover = prepareBackGroundImageSource(homePageImage, {
      dir: options.inputFolder,
      ...options,
    }, topLevel);
    
    return minifyHtml(`<!doctype html>
<html lang="${lang}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="${description}">
    <meta name="keywords" content="${Object.entries(categories).map(
        function ([categoryName, postsFromCategory]) {
        const postsFromCategoryAndLang = postsFromCategory.filter(function (post) {
          return post.lang === lang;
        });
        const {length} = postsFromCategoryAndLang;
        if (!length) {
          return false;
        }
        return translate(lang,  categoryName).split(` `).map(part => {
            return part.split(`-`);
        });
      }).filter(Boolean).flat(Infinity).join(`,`)}">
    <meta name="author" content="${author}">
    <title>${options.tabTitle}</title>
    ${styleSheets(options, topLevel)}
    <meta property="og:site_name" content="${options.mainTitle}">
    <meta property="og:url" content="${options.url}${indexLink(lang, defaultLang, options)}">
    <meta property="og:type" content="site">
    <meta property="og:title" content="${options.mainTitle} - index">
    <meta property="og:description" content="${description}">
    <meta property="og:image" content="${options.url}${openGraphImage(homePageImage)}">
  </head>
  <body>
    ${createHeaderHtml({ ...options, topLevel })}
    <header class="masthead position-relative">
        ${cover}
        <div class="container position-absolute">
            <div class="row">
                <div class="col-lg-8 col-md-10">
                    <div class="site-heading">
                    <h1>${options.mainTitle}</h1>
                    <h2 class="subheading">${options.subTitle}</h2>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="container">
      <div class="row">
        <div class="col-md-7 col-sm-12">
            <h3>${translate(lang, `Posts`)}</h3>
            ${htmlListFromPosts(posts, {lang, topLevel, extensionLinks})}
        </div>
        <div class="col-md-5 col-sm-8">
          <h3>${translate(lang, `Categories`)}</h3>
          <ul>
            ${Object.entries(categories).map(function ([categoryName, postsFromCategory]) {
              const postsFromCategoryAndLang = postsFromCategory.filter(function (post) {
                return post.lang === lang;
              });
              const {length} = postsFromCategoryAndLang;
              if (!length) {
                return ``;
              }
              return `<li>${linkFromCategory(categoryName, options)} (${length})</li>`;
            }).join(``)}
          </ul>
        </div>
      </div>
    </main>
    ${createFooterHtml({ ...options, topLevel: true })}
  </body>
</html>
`);
};

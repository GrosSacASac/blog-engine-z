export { createHeaderHtml };
import {aboutNameStart, contactNameStart, indexLink} from "./links.js";
import { translate } from "../translations/translate.js";
import * as urls from "../defaultSettings/urls.js";

const createHeaderHtml = (options) => {
    const {searchCode, topLevel, lang, defaultLang, extensionLinks} = options;
    let prePath;
    if (topLevel) {
        prePath = `.`;
    } else {
        prePath = `..`;
    }
    return `
    <nav id="top-nav" class="container-fluid">
      <ul class="row">
        <li class="col-lg-3 col-sm-6">
          <a class="nav-link home-link" href="${prePath}/${indexLink(lang, defaultLang, options)}">${translate(lang, `Home`)}</a>
        </li>
        <li class="col-lg-3 col-sm-6">
          <a class="nav-link" href="${prePath}/${urls.posts}${aboutNameStart(lang, defaultLang)}${extensionLinks}">${translate(lang, `About`)}</a>
        </li>
        <li class="col-lg-3 col-sm-6">
          <a class="nav-link" href="${prePath}/${urls.posts}${contactNameStart(lang, defaultLang)}${extensionLinks}">${translate(lang, `Contact`)}</a>
        </li>
        <script type="module">${searchCode}</script>
        <li class="col-lg-3 col-sm-6" id="searchBlock" hidden>
          <label>
            <span id="searchLabel" class="sr-only">${translate(lang, `Search`)} :</span>
            <input id="searchInput" placeholder="${translate(lang, `Search`)}">
          </label>
          <ol id="searchResults"></ol>
        </li>
        
      </ul>
    </nav>`;


};

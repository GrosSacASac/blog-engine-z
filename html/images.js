export { makeBackGroundPicture, makePostImagePicture, openGraphImage, prepareBackGroundImageSource };

import { shouldScaleImage } from "../source/media/image/scalables.js";
import { getMimeFromPath } from "../source/safe-mime.js";
import {
    imageDirectory,
} from "../defaultSettings/paths.js";
import * as resolutions from "../defaultSettings/resolutions.js";
import { createHandleRemoteSource } from "../source/media/handleRemoteSource.js";


const makeBackGroundPicture = (name, {topLevel, lazy}, className = ``, className2 = ``, alt = ``) => {
    let prePath;
    if (topLevel) {
        prePath = `.`;
    } else {
        prePath = `..`;
    }
    let lazyLoading = ``;
    if (lazy) {
        lazyLoading = `loading="lazy"`;
    }
    const mime = getMimeFromPath(name);
    if (!shouldScaleImage(mime)) {
        return `<img alt="${alt}" src="${prePath}/${imageDirectory}${name}" class="${className2}">`;
    }
    // order matters ! first one is picked
    return `<picture class="${className}">
    <source srcset="${prePath}/${imageDirectory}${name}" media="(min-width: ${resolutions.xxl}px)">
    <source srcset="${prePath}/${imageDirectory}${resolutions.standard}-${name}" media="(min-width: ${resolutions.lg}px)">
    <img alt="${alt}" src="${prePath}/${imageDirectory}${resolutions.small}-${name}" class="${className2}" ${lazyLoading}>
</picture>`;
};


const openGraphImage = (name) => {    
    return `${imageDirectory}${resolutions.small}-${name}`;
};

const makePostImagePicture = (parsed, alt, lazy) => {
    const { name, ext } = parsed;
    return makeBackGroundPicture(`${name}${ext}`, {topLevel: false, lazy}, ``, ``, alt);
};

const prepareBackGroundImageSource = (coverImage, options, topLevel) => {
    
    let cover = ``;
    if (coverImage) {
        if (coverImage === `post`) {
            coverImage = `post.jpg`;
        }
        const iref = [0];
        const handleRemoteSource = createHandleRemoteSource(options, iref, `cover-image`);
        const {newName} = handleRemoteSource(coverImage);
        cover = makeBackGroundPicture(newName || coverImage, {topLevel}, `position-relative`, `w100 hauto`);
    }
    return cover;
};

export {styleSheets, highlightJsStyles};


const styleSheets = (options, topLevel) => {
    let prePath;
    if (topLevel) {
        prePath = `.`;
    } else {
        prePath = `..`;
    }
    let custom = ``;
    if (options.customCss) {
        custom = `<link href="${prePath}/css/${options.customCss}" rel="stylesheet">`;
    }
    return `
    <link media="print" href="${prePath}/css/print.css" rel="stylesheet">
    <link media="screen" href="${prePath}/css/blog-engine-sac.min.css" rel="stylesheet">
    ${custom}`;
};


const highlightJsStyles = `<link href="../css/tomorrow.css" rel="stylesheet" media="(prefers-color-scheme: light)">
<link href="../css/solarized-dark.css" rel="stylesheet" media="(prefers-color-scheme: dark)">`;


export {createPostHtml};

import path from "node:path";
import slugify from "@sindresorhus/slugify";
import { concatAsStream } from "stream-sac/source/concatAsStream.js";
import { HtmlMinifier } from "stream-sac/source/html/HtmlMinifier.js";
import isRelativeUrl from "is-relative-url";
import { highlightJsStyles, styleSheets } from "./styles.html.js";
import { createFooterHtml } from "./footer.html.js";
import { createHeaderHtml } from "./header.html.js";
import { linkFromPost, linkFromCategory, postStartName } from "./links.js";
import * as urls from "../defaultSettings/urls.js";
import {makeBackGroundPicture, openGraphImage, prepareBackGroundImageSource} from "./images.js";
import { translate } from "../translations/translate.js";
import { niceDateString } from "../source/dates.js";
// import { minifyHtml } from "./minifyHtml.js";
import { getMimeFromPath, getExtensionFromMime } from "../source/safe-mime.js";
import {
    imageDirectory,
} from "../defaultSettings/paths.js";
import { createHandleRemoteSource } from "../source/media/handleRemoteSource.js";



const createPostHtml = async (options) => {
    const { getHtmlBody } = options;
    
    const { title, author, translations, lang, standalone, description } = options;
    const { coverImage } = options;

    if (standalone) {
      return concatAsStream([getHtmlBody()]).pipe(new HtmlMinifier());
    }
    const commentSection = await createCommentSection(options);
    let lastEdit;
    const topLevel = false;

    if (options.modifiedDate.getTime() === options.creationDate.getTime()) {
        lastEdit = ``;
    } else {
        lastEdit = `<p class="meta">${translate(lang, `Last edit`)} <time datetime="${options.modifiedDate.toJSON()}">${niceDateString(options.modifiedDate, lang)}</time></p>`;
    }

    let availableTranslations = ``;
    if (translations && translations.length) {
        availableTranslations = `${translate(lang, `Translations`)} <ul>${translations.map(({post}) => {
            return `<li>[${post.lang}] ${linkFromPost(post, `hreflang="${post.lang}"`, options)}</li>`;
        }).join(``)}</ul>`;
    }
    const selfUrl = `${options.url}${urls.posts}${postStartName(options)}${options.extensionLinks}`;
    const canonicalUrl = options.canonicalUrl || selfUrl;
    const canonicalMeta = `<link rel="canonical" href="${canonicalUrl}">`;
    let indexMeta = ``;
    if (!options.indexed) {
      indexMeta = `<meta name="robots" content="noindex">`;
    }
    let categoriesHtml = ``;
    if (options.categories.length) {
      categoriesHtml = `
      <h3>${translate(lang, `Categories`)}</h3>
      <ul>${
          options.categories.map(function (category) {
              return `<li>${linkFromCategory(category, options, `.`)}</li>`;
          }).join(``)
      }</ul>`;
    }
    let authorHtml = author;
    if (!isRelativeUrl(author)) {
      try {
        const parserdUrl = new URL(author);
        authorHtml = `<a href="${author}">${parserdUrl.host}${parserdUrl.pathname}</a>`;
      } catch (ignored) {
        console.warn(`could not parse url ${author}`);
      }
      
    }
    const cover = prepareBackGroundImageSource(coverImage, options, topLevel);
    
    return concatAsStream([`<!doctype html>
<html lang="${lang}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="${author}">
    <meta name="description" content="${description}">
    <meta name="keywords" content="${options.searchTags.join(`,`)}">
    ${canonicalMeta}
    ${indexMeta}
    <title>${title}</title>
    ${styleSheets(options, topLevel)}
    ${highlightJsStyles}
    <meta property="og:site_name" content="${options.mainTitle}">
    <meta property="og:url" content="${selfUrl}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="${title}">
    <meta property="og:description" content="${description}">
    <meta property="og:image" content="${options.url}${openGraphImage(coverImage)}">
  </head>

  <body>
    ${createHeaderHtml({ ...options, topLevel })}
    <header class="masthead position-relative">
    ${cover}
    <div class="container position-absolute">
        <div class="row">
          <div class="col-lg-10 col-md-12">
            <div class="site-heading">
              <h1>${title}</h1>
              <p class="meta">${translate(lang, `Posted by`)}
                ${authorHtml}
                <time datetime="${options.creationDate.toJSON()}">${niceDateString(options.creationDate, lang)}</time>
              </p>${lastEdit}
            </div>
          </div>
        </div>
      </div>
    </header>

    <main class="container">
        <div class="row">
          <div class="col-xl-10 col-lg-12">          
            ${availableTranslations}
          </div>
          <article class="col-xl-10 col-lg-12">`,
            getHtmlBody(),
          `</article>
        </div>
    </main>
    <nav class="container post-article-nav">
        <div class="row">
          ${nextInteractionHtml(options)}
          <div class="col-lg-5 col-md-12">${categoriesHtml}
          </div>
        </div>
    </nav>
    ${commentSection}
    ${createFooterHtml({ ...options, topLevel: false })}
    ${addJs(options)}
  </body>
</html>
`]).pipe(new HtmlMinifier());
};

const addJs = (options) => {
  if (typeof options.js === `string`) {
    options.js = [options.js];
  }
  return options.js.map(moduleSource => {
    return `<script type="module" src="${moduleSource}"></script>`;
  }).join(`\n`);
};

const createCommentSection = (options) => {
    const {commentSectionBaseUrl, base2, commentsDisabled, commentsEnabled, commentsRulesUrl, indexed} = options;
    if (!commentsRulesUrl || !commentSectionBaseUrl || !commentsEnabled && (commentsDisabled || !indexed)) {
        return Promise.resolve(``);
    }
    return fetch(commentSectionBaseUrl, {
        method: `POST`,
        headers: {
            Accept: `text/html`,
            [`Content-Type`]: `application/json`,
            [`Accept-Language`]: options.lang,
        },
        body: JSON.stringify({
          slug: postStartName(options),
          folder: options.folder,
          rulesUrl: commentsRulesUrl[options.lang] || commentsRulesUrl[options.defaultLang] || commentsRulesUrl[`default`],
        }),
    }).then(response => {
        if (!response.ok) {
            console.warn(response.status, response.statusText);
            return `<!-- ${response.status}, ${response.statusText} -->`;
        }
        return response.text();
        }).then(htmlCode => {
            return `
            <nav class="container">
                <div class="row">
                    <div class="col-md-10">
                        ${htmlCode}
                    </div>
                </div>
            </nav>
            `;
    }).catch(error => {
      console.warn(error);
      return ``;
    });
};

const nextInteractionHtml = (post) => {
  const {next, previous, indexed, listed, lang, editLink} = post;
  let nextHtml = ``;
  if (next) {
      nextHtml = `<p>${translate(lang, `Next`)} ${linkFromPost(next, `rel="next"`, post)}</p>`;
  }
  let previousHtml = ``;
  if (previous) {
      previousHtml = `<p>${translate(lang, `Previous`)} ${linkFromPost(previous, `rel="prev"`, post)}</p>`;
  }
  let indexedHtml = ``;
  if (!indexed) {
      indexedHtml = `<p>${translate(post.lang, `Not Indexed`)}</p>`;
  } else if (!listed) {
      indexedHtml = `<p>${translate(post.lang, `Unlisted`)}</p>`;
  }
  let editHtml = ``;
  if (editLink) {
    editHtml = `<p><a href="${editLink}">📝 ${translate(post.lang, `Edit`)}</a></p>`;
  }

  return `<div class="col-lg-7 col-md-12">
      ${indexedHtml}
      ${previousHtml}
      ${nextHtml}
      ${editHtml}
  </div>`;
};

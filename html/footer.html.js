export {createFooterHtml};
import licenses from "@ovyerus/licenses";
import { atomFeedPath, jsonFeedPath, rssFeedPath } from "../defaultSettings/paths.js";
import {translate} from "../translations/translate.js";
import {indexLink} from "./links.js";


const createFooterHtml = (options) => {
    const { footerText, defaultLicense, license, langUsed, lang, defaultLang, topLevel } = options;
    // footerText is html,
    let licenseHtml;
    let htmlCode;
    const licenseUsed = license || defaultLicense;

    if (licenses[licenseUsed]) {
        htmlCode = `<a href="${licenses[licenseUsed].url}">${licenses[licenseUsed].name}</a>`;
    } else {
        htmlCode = licenseUsed;
    }
    if (license) {
        licenseHtml = `${translate(lang, `licensed under`)} ${htmlCode}`;
    } else {
        // keep newlines and spacing as is
        licenseHtml = `${translate(lang, `Unless otherwise stated, the content is licensed under`)} ${htmlCode}`;
    }
    let otherLangLinks;
    let prePath;
    if (topLevel) {
        prePath = `.`;
    } else {
        prePath = `..`;
    }
    if (langUsed.size === 1) {
        otherLangLinks = ``;
    } else {
        const links = [];
        langUsed.forEach(aLang => {
            links.push(`<a class="" href="${prePath}/${indexLink(aLang, defaultLang, options)}" hreflang="${aLang}">[${aLang}] ${translate(aLang, `Home`)}</a>`);           
        });
        otherLangLinks = `<div class="col-lg-4 col-md-12">
            <ul>${links.map(link => {
                return `<li>${link}</li>`;
            }).join(``)}</ul>
        </div>`;
    }
    return `
    <footer class="container site-footer">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <p>${licenseHtml}</p>
                ${footerText}
            </div>
           ${otherLangLinks}
           <div class="col-lg-4 col-md-12">
                <ul>
                    <li><a href="${prePath}/${rssFeedPath}" rel="alternate" type="application/rss+xml"><img height="32" width="32" src="${prePath}/rss_logo.png" class="rss logo"> RSS</a></li>
                    <li><a href="${prePath}/${jsonFeedPath}" rel="alternate" type="application/feed+json">JSON feed</a></li>
                    <li><a href="${prePath}/${atomFeedPath}" rel="alternate" type="application/atom+xml">Atom</a></li>
                </ul>
                <link href="${prePath}/${rssFeedPath}" rel="alternate" type="application/rss+xml" title="${options.mainTitle}">
                <link href="${prePath}/${atomFeedPath}" rel="alternate" type="application/atom+xml" title="${options.mainTitle}">
                <link href="${prePath}/${jsonFeedPath}" rel="alternate" type="application/feed+json" title="${options.mainTitle}">
           </div>
        </div>
    </footer>
`;
};

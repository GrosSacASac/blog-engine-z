export { htmlListFromPosts };
import {translate} from "../translations/translate.js";
import { makeUrl, escapeHtml, postStartName } from "./links.js";
import {niceDateString} from "../source/dates.js";

const htmlListFromPosts = function (posts, {lang, topLevel, extensionLinks}) {
    
    let prePath;
    if (topLevel) {
        prePath = ``;
    } else {
        prePath = `.`;
    }
    return posts.filter(function (post) {
        return post.lang === lang;
    }).map(function (post) {
        // todo deduplicate with linkFromPost take regards to topLevel
        return `<a href="${prePath}${makeUrl(postStartName(post))}${extensionLinks}" class="post-preview">
    <span class="post-title">
        ${escapeHtml(post.title)}
    </span>
    <p class="post-meta">${translate(lang, `Posted by`)}
    ${post.author}
    <time datetime="${post.creationDate.toJSON()}">${niceDateString(post.creationDate, lang)}</time>
    </p>
    </a>`;
    }).join(``);
};

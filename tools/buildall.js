//spacu
// import buildjs from "./buildjs.js";
// import buildhtml from "./buildhtml.js";
import { writeTextInFile } from "filesac";
import buildcss from "./buildcss.js";
import packagejson from "../package.json" assert { type: "json" };
// import fsPromises from "node:fs/promises";
// import buildAssets from "./buildAssets.js";
// import cleanup from "./cleanup.js";


const cliInputs = process.argv.slice(2); // command line inputs
const { version } = packagejson;

const JS = "JS";
const HTML = "HTML";
const CSS = "CSS";
const ASSETS = "ASSETS";
const CLEAN = "CLEAN";
const minification = cliInputs.includes("minification=true");

if (cliInputs.length === 0) {
    console.warn("Pass arguments for something to happen");
}



// const packageText =('./package.json');
// const { version } = JSON.parse(packageText);
const options = { 
    minification,
    //  version
};

// if (cliInputs.includes(HTML)) {
//     buildhtml(options);
// }

if (cliInputs.includes(CSS)) {
    buildcss(options);
}

// if (cliInputs.includes(ASSETS)) {
//     buildAssets(options);
// }

// if (cliInputs.includes(JS)) {
//     buildjs(options).then(function () {
//         if (cliInputs.includes(CLEAN)) {
//             cleanup();
//         }
//     }).catch((error) => {
//         console.error(`buildjs did not finish`, error);
//     });
// }
await writeTextInFile("./blog-template/version.txt", `${version}
Version of blog-engine-sac when template was copied`);

export {
    imageDirectory,
    audioDirectory,
    videoDirectory,
    cssDirectory,
    jsDirectory,
    jsonFeedPath,
    atomFeedPath,
    rssFeedPath,
};

const imageDirectory = `images/`;
const audioDirectory = `audio/`;
const videoDirectory = `videos/`;
const cssDirectory = `css/`;
const jsDirectory = `js/`;
const jsonFeedPath = `feed.json`;
const atomFeedPath = `feed.atom`;
const rssFeedPath = `rss.xml`;



export {
    xxl,
    xl,
    lg,
    md,
    sm,

    standard,
    standardWidth,
    small,
    smallWidth,
};

/*from css
    --breakpoint-xs: 0;
    --breakpoint-sm: 576px;
    --breakpoint-md: 768px;
    --breakpoint-lg: 992px;
    --breakpoint-xl: 1200px;
    --breakpoint-xxl: 2000px; */

const xxl = 2000;
const xl = 1200;
const lg = 992;
const md = 768;
const sm = 576;

// image resolutions


const standardWidth = 1080;
const standard = `m`;
const smallWidth = 720;
const small = `s`;

export {
    defaultBuildOptions,
};


const defaultBuildOptions = {
    allowedAccess: `/`,
    inputFolder: `./`,
    outputFolder: undefined,
    deletePreviousOutput: true,
    resizeImages: true,
    fetchRemoteMedia: true,
}

export {fetchWithCache};

import makeFetchHappen from "make-fetch-happen";
import {parseCli} from "../source/parseCli.js";


const cliOptions = parseCli();


const fetchWithCache = makeFetchHappen.defaults({
    cacheManager: `${cliOptions.inputFolder}deletable_cache`, // path where cache will be written (and read)
});

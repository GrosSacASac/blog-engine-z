export { parseCli };

import {parseCli as genericParseCli} from "cli-sac";
import { defaultBuildOptions } from "../defaultSettings/defaultBuildOptions.js";


const parseCli = function() {  
    const cliObject = Object.assign(
        {},
        defaultBuildOptions,
        genericParseCli(),
    );
    
    if (!cliObject.inputFolder.endsWith(`/`) || !cliObject.inputFolder.endsWith(`\\`)) {
        cliObject.inputFolder = `${cliObject.inputFolder}/`;
    }
    if (!cliObject.outputFolder) {
        cliObject.outputFolder = `${cliObject.inputFolder}result/`;
    }
    if (!cliObject.outputFolder.endsWith(`/`) || !cliObject.outputFolder.endsWith(`\\`)) {
        cliObject.outputFolder = `${cliObject.outputFolder}/`;
    }

    

    return cliObject;
};

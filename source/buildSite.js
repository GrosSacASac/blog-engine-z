/* stats.birthtime, can be later than modified date
stats.mtime, for example when file is copy pasted creation date is now while
modified date is when original file was last changed
(it can be earlier when copy pasted) */
export { buildSite };

import url from "node:url";
import fs from "node:fs";
import fsPromises from "node:fs/promises";
import path from "node:path";
import { finished } from "node:stream/promises";
import { 
    writeTextInFile,
    namesInDirectory,
    createNecessaryDirectoriesSync,
    namesInDirectoryRecursive,
} from "filesac";
import { somePromisesParallel } from "utilsac";
import { load } from '@grossacasacs/left-phalange';
import { renderMarkdown } from "./processors/renderMarkdown.js";
import { scaleImagesFromTo } from "./media/image/scaleImage.js";
import { /*generateTags, wordCount,*/ setFinalTags } from "./tags.js";
import { normalizeDate } from "./dates.js";
import { staticCopies } from "./staticCopies.js";
import {
    atomFeedPath,
    imageDirectory, jsonFeedPath, rssFeedPath,
} from "../defaultSettings/paths.js";
import { generateSearchCode } from "./search.js";
import { postFromFileDetails, getDetailsFromPost, getPostDefault } from "./buildPosts.js";
import { processMetaFile } from "./meta.js";
import { consoleOutputEarly, consoleOutputReady } from "./reporter/consoleOutput.js";
import {categoryFileName, indexFileName, postStartName} from "../html/links.js";
import * as urls from "../defaultSettings/urls.js";


import { createPostHtml } from "../html/post.html.js";
import { createIndexHtml } from "../html/index.html.js";
import { createCategoryHtml } from "../html/category.html.js";
import { translations } from "../translations/translations.js";
import { createSlidePages } from "./processors/slides.js";
import { fetchWithCache } from "../defaultSettings/fetchCache.js";
import { feedString } from "./rss.js";


const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// sources
const defaultOptionsSource = `${__dirname}/../defaultSettings/default-blog-engine-sac`;



const createCategoryMap = function (posts, categoriesList) {
    /*[{
        "tag": "travel",
        "name": "Travel"}] */
    const categories = {};
    categoriesList.forEach(({tag}) => {
        categories[tag] = [];
    });
    posts.forEach(function (post) {
        post.finalTags.forEach(function (tag) {
            if (Object.hasOwn(categories, tag)) {
                categories[tag].push(post);
            }
        });
    });
    return categories;
};

const createHiddenCategoryMap = function (posts, categoryMap) {
    /*[{
        "tag": "travel",
        "name": "Travel"}] */
    const hiddenCategoryMap = {};
    
    posts.forEach(function (post) {
        post.finalTags.forEach(function (tag) {
            if (Object.hasOwn(categoryMap, tag)) {
                return;
            }
            if (!Object.hasOwn(hiddenCategoryMap, tag)) {
                hiddenCategoryMap[tag] = [];
            }
            hiddenCategoryMap[tag].push(post);
        });
    });
    return hiddenCategoryMap;
};

const createCategoryPages = function (categoriesMap, hiddenCategoryMap, commonOptions, categoriesPath) {
    const allCategories = Object.assign({}, categoriesMap, hiddenCategoryMap);
    return somePromisesParallel(
        Object.entries(allCategories).map(function ([categoryName, posts]) {
            return function () {
                if (!posts.length) {
                    return;
                }
                return Promise.all(Array.from(commonOptions.langUsed).map(lang => {
                    const categoryHTML = createCategoryHtml(Object.assign({
                        categoryName,
                        posts,
                    }, commonOptions,{lang}));
                    return writeTextInFile(`${categoriesPath}${categoryFileName(categoryName, lang, commonOptions.defaultLang)}`,
                        categoryHTML,
                    );

                }));

            };
        }),
    );
};

const createIndex = function (posts, categories, lang, commonOptions, outputFolder) {
    const indexHTML = createIndexHtml(Object.assign({}, commonOptions, {
        posts,
        categories,
        lang,
    }));
    
    const indexPath = `${outputFolder}${indexFileName(lang, commonOptions.defaultLang)}`;
    return writeTextInFile(indexPath, indexHTML);
};


const createIndexes = function (listedPosts, categories, langs, commonOptions, outputFolder) {
    return Promise.all(Array.from(langs).map(lang => {
        return createIndex(listedPosts, categories, lang, commonOptions, outputFolder);
    }));
};



const createPosts = function (posts, commonOptions, blogPath) {
    createNecessaryDirectoriesSync(`${blogPath}x.html`);
    return somePromisesParallel(posts.map(function (post) {
        return function () {
            return createPostHtml(Object.assign({}, commonOptions, post)).then(function (postInsideHTML) {
                
                const dest = fs.createWriteStream(`${blogPath}${postStartName(post)}.html`);
                postInsideHTML.pipe(dest);
                return finished(dest);
            });
        };
    }));
};

const addTranslatedCategories = function (categories) {
    categories.forEach(category => {
        Object.entries(category).forEach(([key, value]) => {
            if (key === `tag`) {
                return;
            }
            let translationsForThatLang = translations[key];
            if (!translationsForThatLang) {
                translations[key] = {};
                translationsForThatLang = translations[key];
            }
            translationsForThatLang[category.tag] = value;
        });
    });
};

const buildSite = async (buildOptions) => {
    buildOptions.allowedAccess = path.resolve(buildOptions.allowedAccess || `/`);
    const {
        allowedAccess,
        inputFolder,
        outputFolder,
        deletePreviousOutput,
        resizeImages,
        fetchRemoteMedia,
        publicLogger = console,
        commentsHeaders = {},
    } = buildOptions;
    const startTimeRelative = performance.now();
    const userOptionsSource = `${inputFolder}blog-engine-sac`;
    const postsPath = `${inputFolder}source/`;
    const extrasSource = `${inputFolder}extras/`;
    const footerSource = `${extrasSource}footer.md`;
    
    // output
    const imagesOut = `${outputFolder}${imageDirectory}`;
    const blogPath = `${outputFolder}${urls.posts}`;
    const categoriesPath = `${outputFolder}${urls.categories}`;
    const [sourceFileNames, footerText, userOptions, defaultOptions] = await Promise.all([
        namesInDirectoryRecursive(postsPath),
        fsPromises.readFile(footerSource, `utf-8`),
        load(userOptionsSource),
        load(defaultOptionsSource),
    ]);

    
    const folder = path.basename(inputFolder);
    const commonOptions = {
        ...defaultOptions,
        ...userOptions,
        inputFolder,
        folder,
        postsPath,
        outputFolder,
    };

    commonOptions.extensionLinks = commonOptions.htmlExtensionLinks && `.html` || ``;
    addTranslatedCategories(commonOptions.categories);
    
    // todo there is a bug when resizing resized images
    // and resieImage will scale image from the output
    if (deletePreviousOutput || resizeImages /*todo*/) {
        try {
            fs.rmSync(outputFolder, { recursive: true, force: true });
        } catch (error) {
            console.error(error);
            console.error(`Error while deleting ${outputFolder}`);
        }
    }
    const staticPromise = staticCopies({
        outputFolder,
        inputFolder,
        customCss : commonOptions.customCss,
    });

    const footerHTML = renderMarkdown(footerText);
    commonOptions.footerText = footerHTML;
    commonOptions.tabTitle = commonOptions.tabTitle || commonOptions.mainTitle;
    commonOptions.lang = commonOptions.defaultLang;

    const sourceFilesPath = sourceFileNames.map((file) => {
        return `${postsPath}${file}`;
    });
    const detailedSources = sourceFilesPath.map(filePath => {
        return getDetailsFromPost(filePath);
    });

    /* pass this array so that postFromFileDetails can use meta information of previous posts*/
    const metaArray = (await Promise.all(detailedSources.map(processMetaFile))).filter(Boolean);
    const posts = (await somePromisesParallel(detailedSources.map((detailedSource) => {
        return function () {
            return postFromFileDetails(detailedSource, metaArray, commonOptions);
        };
    }))).flat(Infinity)
    .filter(Boolean).filter(post => {
        return post.getContentPromise;
    }).filter(post => {
        return post.src && !post.resolvedPath;
    }).map(consoleOutputEarly.bind(undefined, publicLogger)).filter(post => {
        return post.ready !== false;
    });

    
   
    const linkedTranslations = new Set();
    const langUsed = new Set([commonOptions.defaultLang]);

    posts.forEach(post => {
        // filter translations
        posts.some(postWithTranslation => {
            return post !== postWithTranslation && postWithTranslation?.translations?.some(translation => {
                if ((path.parse(translation.src).name !== post.base) && (translation.src !== post.src)) {
                    return false;
                }
                linkedTranslations.add(postWithTranslation);
                const externalMeta = postWithTranslation.externalMeta || postWithTranslation;
                const temp = Object.assign({}, externalMeta, post);
                Object.assign(post, temp);
                Object.assign(post, translation);
                delete post[`translations`];
                post.isTranslation = true;
                post.originalBase = postWithTranslation.base;
                translation.post = post;
                return true;
            });
        });
    });

    posts.forEach(({lang}) => {
        langUsed.add(lang);
    });
    
    posts.forEach(post => {
        Object.assign(post, Object.assign(getPostDefault(post), post));
        if (post.folderCategory) {
            if (!commonOptions.categories.includes(post.folderCategory)) {
                commonOptions.categories.push({tag: post.folderCategory});
            }
            if (!post.categories.includes(post.folderCategory)) {
                post.categories.push(post.folderCategory);
            }
        }
    });
    
    /*
    const allWords = wordCount(posts);
    generateTags(posts, allWords);
    */
    setFinalTags(posts);
    
    const listedPosts = posts.filter(post => {
        return post.listed && post.indexed;
    });    
    const indexedPosts = posts.filter(post => {
        return post.indexed;
    });
    listedPosts.forEach(function (post, i) {
        post.next = listedPosts[i - 1];
        post.previous = listedPosts[i + 1];
    });


    linkedTranslations.forEach(postWithTranslations => {
        const {title, lang} = postWithTranslations;
        const translationObject = {
            lang,
            title,
            post: postWithTranslations,
        };

        postWithTranslations.translations.forEach(nonLinkedYetLink => {
            const nonLinkedYet = nonLinkedYetLink.post;

            nonLinkedYet.translations = postWithTranslations.translations.filter(a => {
                return a.post !== nonLinkedYet;
            }).concat(translationObject);
        });
    });

    posts.forEach(post => {
        if (!(post.creationDate instanceof Date)) {
            post.creationDate = new Date(post.creationDate);
        }
        if (!(post.modifiedDate instanceof Date)) {
            post.modifiedDate = new Date(post.modifiedDate);
        }
    });

    
    posts.sort((a, b) => {
        return Number(a.indexed) - Number(b.indexed);
    });
    posts.forEach(consoleOutputReady.bind(undefined, publicLogger, commonOptions));
    listedPosts.sort(function (postA, postB) {
        const postADate = normalizeDate(postA.creationDate);
        const postBDate = normalizeDate(postB.creationDate);
        return -(postADate - postBDate);
    });

    const postsByLang = new Map();
    langUsed.forEach(lang => {
        postsByLang.set(lang, []);
    });
    listedPosts.forEach(post => {
        postsByLang.get(post.lang).push(post);
    });
    
    postsByLang.forEach((postByLang/*, lang*/) => {
        postByLang.forEach(function (post, i) {
            post.next = postByLang[i - 1];
            post.previous = postByLang[i + 1];
        });
    });
    
    let commentsPromise;
    if (commonOptions.commentSetTargets) {
        commentsPromise = fetch(commonOptions.commentSetTargets, {
            method: `POST`,
            body: JSON.stringify({
                folder: commonOptions.folder,
                targets: posts.filter((post) => {
                    return !post.commentsDisabled && (post.indexed || post.commentsEnabled);
                }).map((post) => {
                    // same as in post html .js
                    return {
                        slug: postStartName(post),
                        url: `${commonOptions.url}${urls.posts}${postStartName(post)}${commonOptions.extensionLinks}`,
                    };
                }),
            }),
            headers: {
                ...commentsHeaders,
                'Content-type': `application/json`,
                Accept: `application/json`,
            },
        }).then(response => {
            if (!response.ok) {
                throw response.statusText;
            }
            return response.text();
        }).catch(console.error);
    } else {
        commentsPromise = Promise.resolve(true);
    }
    
    
    commonOptions.searchCode = await generateSearchCode(indexedPosts, commonOptions);
    commonOptions.langUsed = langUsed;
    const categoryMap = createCategoryMap(listedPosts, commonOptions.categories);
    const hiddenCategoryMap = createHiddenCategoryMap(listedPosts, categoryMap);
    const categoriesPromise = createCategoryPages(categoryMap, hiddenCategoryMap, commonOptions, categoriesPath);
    const indexesPromise = createIndexes(listedPosts, categoryMap, langUsed, commonOptions, outputFolder);

    // set targets before requesting the form
    await commentsPromise;
    // await for medias to be populated
    await createPosts(posts, commonOptions, blogPath);

    const allMedias = [];
    posts.forEach(post => {
        if (!post.medias || !post.ready) {
            return;
        }
        // console.log(`${post.title} medias used:`)
        // console.log(post.medias);
        allMedias.push(...post.medias);
    });

    // will create folders
    await staticPromise;
    let remoteMediaPromise = Promise.resolve(true);

 

    if (fetchRemoteMedia) {
        remoteMediaPromise = somePromisesParallel(allMedias.map((media, i) => {
            if (!media.original && ! media.originalLocal) {
                return function () {
                };
            }
            if (media.originalLocal) {
                return function () {
                    const resolvedSource = media.originalLocal;
                    const allowed = resolvedSource.startsWith(allowedAccess);
                    if (!allowed) {
                        console.warn(`trying to access not allowed ${resolvedSource} outside of ${allowedAccess}`);
                        return;
                    }
                    return fsPromises.copyFile(resolvedSource, media.needsToBePlacedHere).catch(function (probablyNotFoundError) {
                        publicLogger.log(`Error ${resolvedSource} not found`);
                    });
                };
            }
            return function () {
                publicLogger.log(`fetching remote media (${i + 1}/${allMedias.length})`);
                return fetchWithCache(media.original).then(res => {
                    const dest = fs.createWriteStream(media.needsToBePlacedHere);
                    return finished(res.body.pipe(dest));
                }).catch(function (error) {
                    console.error(error);
                    console.warn(`could not fetch ${media.original}`);
                });
            };
        }));
    }

    // do after remoteMediaPromise to also resize those
    
    await remoteMediaPromise;
    // remote image land in result/image directly, resize from there with rest at once
    
    
    let imageResizePromise = Promise.resolve(true);
    if (resizeImages) {
        imageResizePromise = scaleImagesFromTo(imagesOut, imagesOut, publicLogger);
    }

    createNecessaryDirectoriesSync(`${commonOptions.outputFolder}${urls.slides}x.html`);
    const slidePagesPromise = somePromisesParallel(posts.map(post => {
        return function () {
            return createSlidePages(post, commonOptions);
        };
    }));

    const {feedJson1, atom1, rss2}  = feedString(commonOptions, listedPosts);
    const feedPromise = Promise.all([
        writeTextInFile(`${outputFolder}/${jsonFeedPath}`, feedJson1),
        writeTextInFile(`${outputFolder}/${atomFeedPath}`, atom1),
        writeTextInFile(`${outputFolder}/${rssFeedPath}`, rss2),
    ])

    await Promise.all([
        staticPromise,
        imageResizePromise,
        categoriesPromise,
        indexesPromise,
        commentsPromise,
        slidePagesPromise,
        feedPromise,
    ]);
    const duration = performance.now() - startTimeRelative;
    publicLogger.log(`Build Finished ! (${duration.toFixed(0)}ms)`);
};

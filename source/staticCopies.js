export {
    staticCopies,
};

import path from "node:path";
import url from "node:url";
import { copyFile, copyDirectory, createNecessaryDirectoriesSync } from "filesac";
import {
    imageDirectory,
    audioDirectory,
    videoDirectory,
    cssDirectory,
    jsDirectory,
} from "../defaultSettings/paths.js";

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const modules  = `${__dirname}/../node_modules/`;


const staticCopies = function (options) {
    
    return Promise.all([
        copyVideos(options),
        copyAudio(options),
        copyImages(options),
        copyImageAssets(options),
        copyCss(options),
        copyJs(options),
    ]);
};

const copyCss = (options) => {
    const fileToCopy = `/../css/blog-engine-sac.min.css`;
    const printStyles = `/../css/print.css`;
    const { outputFolder } = options;

    return Promise.all([
        copyFile(`${__dirname}${fileToCopy}`, `${outputFolder}${cssDirectory}${path.basename(fileToCopy)}`),
        copyFile(`${__dirname}${printStyles}`, `${outputFolder}${cssDirectory}${path.basename(printStyles)}`),
        copyFile(`${modules}virstellung/source/virstellung.css`, `${outputFolder}${cssDirectory}virstellung.css`),
        copyFile(`${modules}highlight.js/styles/stackoverflow-dark.css`, `${outputFolder}${cssDirectory}solarized-dark.css`),
        copyFile(`${modules}highlight.js/styles/stackoverflow-light.css`, `${outputFolder}${cssDirectory}tomorrow.css`),
        options.customCss && copyCustomCss(options),
    ]);
};

const copyJs = (options) => {
    const fileToCopy = `/../js/virstellung.es.js`;
    const fileToCopy2 = `/../js/virstellungAutoLaunch.es.js`;
    const { outputFolder, inputFolder } = options;

    return Promise.all([
        copyFile(`${__dirname}${fileToCopy}`, `${outputFolder}${jsDirectory}${path.basename(fileToCopy)}`),
        copyFile(`${__dirname}${fileToCopy2}`, `${outputFolder}${jsDirectory}${path.basename(fileToCopy2)}`),
        copyDirectory(`${inputFolder}${jsDirectory}`, `${outputFolder}${jsDirectory}`).catch(function () {}),
    ]);
};


const copyCustomCss = (options) => {
    const { outputFolder, inputFolder } = options;
    return copyDirectory(`${inputFolder}${cssDirectory}`, `${outputFolder}${cssDirectory}`);
};

const copyImages = (options) => {
    const { outputFolder, inputFolder } = options;
    return copyDirectory(`${inputFolder}${imageDirectory}`, `${outputFolder}${imageDirectory}`);
};

const copyImageAssets = (options) => {
    const { outputFolder } = options;
    return copyFile(`${__dirname}/../images/rss_logo.png`, `${outputFolder}rss_logo.png`);
};

const copyAudio = (options) => {
    const { outputFolder, inputFolder } = options;
    
    createNecessaryDirectoriesSync(`${outputFolder}${audioDirectory}x`);
    return copyDirectory(`${inputFolder}${audioDirectory}`, `${outputFolder}${audioDirectory}`).catch(function () {});
};

const copyVideos = (options) => {
    const { outputFolder, inputFolder } = options;

    createNecessaryDirectoriesSync(`${outputFolder}${videoDirectory}x`);
    return copyDirectory(`${inputFolder}${videoDirectory}`, `${outputFolder}${videoDirectory}`).catch(function () {});
};



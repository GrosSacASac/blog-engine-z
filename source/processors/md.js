export { processPostAsMD, formatMD };

import path from "node:path";
import {MarkdownParser} from "stream-sac/source/markdown/MarkdownParserNode.js";
import hljs from "highlight.js";
import isRelativeUrl from "is-relative-url";
import slugify from "@sindresorhus/slugify";
// import {renderMarkdown} from "./renderMarkdown.js";
import { makePostImagePicture, makeBackGroundPicture } from "../../html/images.js";
import { getMimeFromPath, getExtensionFromMime } from "../safe-mime.js";
import {
    imageDirectory,
    audioDirectory,
    videoDirectory,
    cssDirectory,
    jsDirectory,
} from "../../defaultSettings/paths.js";
import { prepareSlides } from "./slides.js";
import { createHandleRemoteSource } from "../media/handleRemoteSource.js";


const formatMD = [`md`, `markdown`];
const processPostAsMD = function (post) {
    post.getHtmlBody = function () {
        const i = [0];
        const handleRemoteSource = createHandleRemoteSource(post, i, `md`);
        let slidesId = 0;
        return post.getContentStream().pipe(new MarkdownParser({
            highlight: function (code, language) {
                if (language && hljs.getLanguage(language)) {
                  try {
                    return hljs.highlight(code, {language}).value;
                  } catch (__) {
                    console.warn(`can not handle highlightjs with ${language}`);
                  }
                }
            
                return false;
            },
            mediaHook: function (source, altText) {
                if (source.startsWith(`slides:`)) {
                    source = JSON.parse(source.substring(`slides:`.length)).map(handleRemoteSource);
                    let styles = ``;
                    if (!post.slides.length) {
                        styles = `<link media="screen" href="../css/virstellung.css" rel="stylesheet">`;
                    }
                    const id = `s-${slidesId}`;
                    slidesId += 1;
                    post.slides.push({id, sources: source});
                    return `${styles}${prepareSlides(post, source, id)}`;

                }
                const temp3 = handleRemoteSource(source);
                source = temp3.source;
                const {parsed} = temp3;
                const directory = parsed.dir;
                const mime = getMimeFromPath(parsed.base);
                if (mime.startsWith(`audio`)) {
                    return `<audio src="${source}" controls>${altText}</audio>`;
                }
                if (mime.startsWith(`video`)) {
                    return `<video src="${source}" controls>${altText}</video>`;
                }
                if (!directory.includes(`images`)) {
                    console.warn(`relative image with source ${source} outside of /images`);
                    return `<img alt="${altText}" src="${source}">`;
                }
                const lazy = (i[0] > 3);
                return makePostImagePicture(parsed, altText, lazy);
            },
        }));
    };
    return post;
    
    
};

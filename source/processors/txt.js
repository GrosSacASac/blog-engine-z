export { processPostAsTXT, formatTXT };

import {escape as escapeHtml} from 'html-escaper';
import { streamifyStringFunction } from "stream-sac/source/streamifyStringFunction.js";
import { concatAsStream } from "stream-sac/source/concatAsStream.js";


const createEscapeHtmlStream = streamifyStringFunction(escapeHtml);

const formatTXT = [`txt`, `text`];

const processPostAsTXT = function (post) {
    post.getHtmlBody = function () {
        const escapedHtmlStream = createEscapeHtmlStream();
        post.getContentStream().pipe(escapedHtmlStream);
        return concatAsStream([`<pre class="txtfile">`, escapedHtmlStream, `</pre>`]);
    };
    return post;
};




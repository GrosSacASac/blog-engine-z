export { processPostAsHTML, formatHTML };

import nodeHtmlParser from "node-html-parser";
const { parse } = nodeHtmlParser;


const formatHTML = [`html`, `htm`];

const processPostAsHTML = function (post) {
    post.getHtmlBody = function () {
        return post.getContentPromise().then(content => {
            return htmlBodyFromContent(content);
        });
    };
    return post;
};

const htmlBodyFromContent = function (content) {
    const htmlContainers = [`html`, `body`, `head`];
    const rootOriginal = parse(content);
    const root = rootOriginal.firstChild;
    let htmlBody;
    if (!htmlContainers.includes(root.tagName)) {
        htmlBody = rootOriginal.innerHTML;
    } else if (root.tagName === `html`) {
        const html = root;
        // const head = html.querySelector(`head`);
        const body = html.querySelector(`body`);
        if (body) {
            htmlBody = body.innerHTML;
        } else {
            htmlBody = root.innerHTML;
        }
    } else if (root.tagName === `body`) {
        const body = root;
        htmlBody = body.innerHTML;
    } else {
        console.warn(`${root.tagName} as root is not supported`);
        htmlBody = ``;
    }

    return htmlBody;
};

export {prepareSlides, createSlidePages};

import fsPromises from "node:fs/promises";
import { virstellung } from "virstellung";
import { getMimeFromPath } from "../safe-mime.js";
import * as urls from "../../defaultSettings/urls.js";
import { makeUrl, postStartName } from "../../html/links.js";
import * as resolutions from "../../defaultSettings/resolutions.js";
import { shouldScaleImage } from "../media/image/scalables.js";
import {
    imageDirectory,
} from "../../defaultSettings/paths.js";


const multipleResolutionImageForSlides = (parsed, mime) => {
    const topLevel = false;
    let prePath;
    if (topLevel) {
        prePath = `.`;
    } else {
        prePath = `..`;
    }
    return [{
        url: `${prePath}/${imageDirectory}${parsed.name}${parsed.ext}`,
        media: `(min-width: ${resolutions.xxl}px)`,
        mime,
    }, {
        url: `${prePath}/${imageDirectory}${resolutions.standard}-${parsed.name}${parsed.ext}`,
        media: `(min-width: ${resolutions.lg}px)`,
        mime,
    }, {
        url: `${prePath}/${imageDirectory}${resolutions.small}-${parsed.name}${parsed.ext}`,
        mime,
    }];
};

const prepareSlides = (post, sources, id) => {
    const generateHref = function (index) {
        return `../${urls.slides}${postStartName(post)}${id}-${index}.html`;
    };
    const preparedSlideItems = sources.map(({source, parsed}) => {
        const itemMime = getMimeFromPath(source);
        if (itemMime.startsWith(`image`) && shouldScaleImage(itemMime)) {
            return {
                label: parsed.name,
                mime: itemMime,
                sources: multipleResolutionImageForSlides(parsed, itemMime),
            };
        }
        return {
            label: parsed.name,
            url: source,
            file: source,
            mime: itemMime,
        };
    });
    
    return `${virstellung({
        slideItems: preparedSlideItems, // array with {label, file, mime}
        currentSlide: 0, // current slide
        generateHref,
        translate: undefined, // optional translation function
        id, // id that will be used in the html,
        // for the event handlers to know which slide should go next
        // (there can be multiples slides on the same page)
        //  only required if multiple slides are used
        getText: undefined, // function used to get the text content (server side)
        // may be async
        // needed because there is no native html include yet
        // the text should be html escaped 
    })}
    <script type="module">
    import {stellFir} from "../js/virstellung.es.js";
    stellFir("${id}");
    </script>`;
};

const createSlidePages = async (post, commonOptions) => {
    if (!post.slides.length) {
        return;
    }
    const {slides} = post;
    return Promise.all(slides.map(slideItem => {
        const {sources, id} = slideItem;
        const generateHref = function (index) {
            return `./${postStartName(post)}${id}-${index}.html`;
        };
        // todo deduplicate
        const preparedSlideItems = sources.map(({source, parsed}) => {
            const itemMime = getMimeFromPath(source);
            if (itemMime.startsWith(`image`) && shouldScaleImage(itemMime)) {
                return {
                    label: parsed.name,
                    mime: itemMime,
                    files: multipleResolutionImageForSlides(parsed, itemMime),
                };
            }
            return {
                label: parsed.name,
                file: source,
                fileAlone: source,
                mime: itemMime,
            };
        });
        // todo deduplicate
        const linkBack = `.${makeUrl(postStartName(post))}${commonOptions.extensionLinks}`;
        const htmlCodeForAllSlides = preparedSlideItems.map((preparedSlideItem, i) => {
            return `<!doctype html><html><head>
            <title>${preparedSlideItem.label}</title>
            <meta name="viewport" content="width=device-width">
            <link media="screen" href="../css/virstellung.css" rel="stylesheet">
            <meta name="robots" content="noindex">
            <style>h1 {padding:0; margin:0}.virstellung{height:85vh}body {
                margin: 0;
            }</style>
            </head><body>
            <h1><a href="${linkBack}">⬅ Go back to original</a></h1>
            ${virstellung({
                slideItems: preparedSlideItems, // array with {label, file, mime}
                currentSlide: i, // current slide
                generateHref,
                translate: undefined, // optional translation function
                getText: undefined, // function used to get the text content (server side)
                // may be async
                // needed because there is no native html include yet
                // the text should be html escaped 
            })}
            <script type="module" src="../js/virstellungAutoLaunch.es.js"></script>
        </body></html>`;
        });
        
        
        return Promise.all(htmlCodeForAllSlides.map((htmlCode, i) => {
            const destination = `${commonOptions.outputFolder}${urls.slides}${postStartName(post)}${id}-${i}.html`;
            
            return fsPromises.writeFile(destination, htmlCode);
        }));
    }));
};

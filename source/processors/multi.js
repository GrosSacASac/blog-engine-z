export { processPostAsMulti, formatMulti };

import fsPromises from "node:fs/promises";
import {createReadStream} from "node:fs";
import { supportedExtensions, parse } from "@grossacasacs/left-phalange";
import { initialPost, processExternalPost, getDetailsFromPost, processPostAs } from "../buildPosts.js";
import { isLocalUrl } from "../urls.js";


const formatMulti = supportedExtensions;
const processPostAsMulti = function (post, commonOptions) {
    // here we are in a json, toml etc file
    // that points to another file, so we have 2 
    
    return post.getContentPromise().then(content => {
        const { fullPath, extension } = post;
        let postObject;
        try {
            postObject = parse(content, extension);
        } catch (error) {
            console.error(`could not parse ${fullPath} as ${extension}`);
            console.error(error);
            return ``;
        }
        const { src } = postObject;
        if (!src) {
            return ``;
        }
        let basePromise;
        if (isLocalUrl(src)) {
            const fileDetails = getDetailsFromPost(`${commonOptions.postsPath}${src}`);
            const {fullPath, extension} = fileDetails;

            const post = initialPost(fileDetails, commonOptions);
            post.getContentPromise = function () {
                return fsPromises.readFile(fullPath, `utf-8`);
            };
            post.getContentStream = function () {
                return createReadStream(fullPath, `utf-8`);
            };
            processPostAs(extension, post, commonOptions);
            Object.assign(post, postObject);
            basePromise = post;
        } else {
            basePromise = processExternalPost({ ...post, ...postObject });
        }
        if (postObject?.translations?.length) {
            return Promise.all(postObject.translations.filter(translation => {

                return !isLocalUrl(translation.src);
            }).map((translation) => {
                return processExternalPost({ ...post, ...postObject, ...translation });
            }).concat(basePromise));
        }
        return basePromise;
    });

};


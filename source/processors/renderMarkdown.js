export { renderMarkdown };
import markdownModule from "markdown-it";
import hljs from "highlight.js";

const markdown = markdownModule({
    html:         true,        // Enable HTML tags in source
    langPrefix:   `language-`,  // CSS language prefix for fenced blocks. Can be
                                // useful for external highlighters.
    linkify:      true,        // Autoconvert URL-like text to links
  
    // Enable some language-neutral replacement + quotes beautification
    // For the full list of replacements, see https://github.com/markdown-it/markdown-it/blob/master/lib/rules_core/replacements.js
    typographer:  false,
  
    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Could be either a String or an Array.
    //
    // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
    // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
    //quotes: '“”‘’',
  
    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed and should be escaped externally.
    // If result starts with <pre... internal wrapper is skipped.
    highlight: function (code, language) {
      if (language && hljs.getLanguage(language)) {
        try {
          return hljs.highlight(code, {language}).value;
        } catch (__) {
          console.warn(`can not handle highlightjs with ${language}`);
        }
      }
  
      return ``;
  },
});


const renderMarkdown = function (md) {
    return markdown.render(md);
};

export {getMimeFromPath, getExtensionFromMime};

import mime from "mime";


const defaultMime = `application/octet-stream`;
const defaultExtension = `txt`;

const getMimeFromPath = (path) => {
    return mime.getType(path) || defaultMime;
};

const getExtensionFromMime = (mimeString) => {
    return mime.getExtension(mimeString) || defaultExtension;
};

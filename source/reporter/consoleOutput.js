import { postStartName } from "../../html/links.js";
import * as urls from "../../defaultSettings/urls.js";

export { consoleOutputEarly, consoleOutputReady };


const consoleOutputEarly = (logger, post) => {
    if (post.ready === false) {
        logger.log(`\t--- not ready ${post.title}`);            
    }
    return post;
};

const consoleOutputReady = function (logger, commonOptions, post) {
    const { indexed, listed, isTranslation } = post;
    if (isTranslation) {
        return;
    }
    const langs = `${[post.lang, ...post.translations.map(translation => {
        return translation.lang;
    })].join(`, `)}`;
    
    let prefix;
    if (indexed && listed) {
        prefix = `+++`;
    } else if (indexed && !listed) {
        prefix = `++- not listed`;
    } else {
        prefix = `+-- not indexed`;
    }
    logger.log(`\t${prefix} ${post.title} [${langs}]`);
    if (!indexed) {
        // todo deduplicate
        const selfUrl = `${commonOptions.url}${urls.posts}${postStartName(post)}${commonOptions.extensionLinks}`;
        logger.log(`\t\tsecret url:
\t\t${selfUrl}`);
    }
};

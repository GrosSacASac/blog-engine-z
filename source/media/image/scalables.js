export { scalables, shouldScaleImage };

// https://sharp.pixelplumbing.com/
const scalables = [
    // cannot output gif
    // should not scale svg
    `jpeg`,
    `png`,
    `webp`,
    `avif`,
    `tiff`,
];

const shouldScaleImage = (mime) => {
    return mime && mime.includes(`image`) && scalables.some(scalable => {
        return mime.includes(scalable);
    });
};


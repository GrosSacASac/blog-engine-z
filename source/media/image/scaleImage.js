export {
    scaleImageStandardAndSmall,
    scaleImagesFromTo,
};

import path from "node:path";
import Sharp from "sharp";
import { namesInDirectoryRecursive } from "filesac";
import { somePromisesParallel } from "utilsac";
import { getMimeFromPath } from "../../safe-mime.js";
import { shouldScaleImage } from "./scalables.js";
import {
    standard,
    standardWidth,
    small,
    smallWidth,
} from "../../../defaultSettings/resolutions.js";



// relative to cwd
// path.resolve(`./images/home.jpg`)

// relative to js file
// path.resolve(`${__dirname}/images/home.jpg`)


/**
will keep folder structure
 */
const scaleImageStandardAndSmall = (inputFolder, imagePath, targetFolder, logger = console) => {
    const imagePathParsed = path.parse(imagePath);
    const imageBase = imagePathParsed.base;
    const mime = getMimeFromPath(imageBase);
    if (!shouldScaleImage(mime)) {
        return Promise.resolve();
    }
    logger.log(`Resizing image ${imageBase}`);
    return new Promise(function (resolve, reject) {
        const image = new Sharp(path.resolve(path.join(inputFolder, imagePath)), {
            limitInputPixels: 10 ** 12,
        });
        image.metadata().then(function (metadata) {
            let standardImage = image;
            if (metadata.width > standardWidth) {
                standardImage = image.resize({
                    width: standardWidth,
                    height: undefined, // scale with width
                });
            }
            imagePathParsed.base = `${standard}-${imageBase}`;
            const standardPromise = standardImage.toFile(`${targetFolder}${path.format(imagePathParsed)}`);
            
            
            let smallImage = image;
            if (metadata.width > smallWidth) {
                smallImage = image.resize({
                    width: smallWidth,
                    height: undefined, // scale with width
                });
            }
            imagePathParsed.base = `${small}-${imageBase}`;
            const smallPromise = smallImage.toFile(`${targetFolder}${path.format(imagePathParsed)}`);
            return Promise.all([standardPromise, smallPromise]);
        }).then(resolve).catch((error) => {
            console.warn(`Problem with image ${imagePath}`);
            reject(error);
        });
    });
};


const scaleImagesFromTo = (inputFolder, targetFolder, logger = console) => {
    return namesInDirectoryRecursive(inputFolder).then(imagePaths => {
        return somePromisesParallel(imagePaths.map((imagePath) => {
            return function () {
                return scaleImageStandardAndSmall(inputFolder, imagePath, targetFolder, logger).catch(console.error);
            };
        }));
    });
};

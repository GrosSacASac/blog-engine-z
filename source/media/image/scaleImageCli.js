import {scaleImagesFromTo} from "./scaleImage.js";
import {parseCli} from "../../parseCli.js";


const {inputFolder, outputFolder} = parseCli();


scaleImagesFromTo(inputFolder, outputFolder, console).then(function () {
    console.log(`Successfully resized images`);
});


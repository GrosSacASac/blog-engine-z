export {createHandleRemoteSource};


import path from "node:path";
import isRelativeUrl from "is-relative-url";
import slugify from "@sindresorhus/slugify";
import { getMimeFromPath, getExtensionFromMime } from "../safe-mime.js";
import {
    imageDirectory,
    audioDirectory,
    videoDirectory,
    cssDirectory,
    jsDirectory,
} from "../../defaultSettings/paths.js";


const createHandleRemoteSource = (post, i, sep = ``) => {return (source) => {
    i[0] += 1;
    if (isRelativeUrl(source) && !post.isRemote) {
        const absoluteSource = path.resolve(path.join(post.dir, source)); // this version will find be relative to the md file
        // const absoluteSource = path.resolve(path.join(path.dirname(post.fullPath), source)); // this one is relative to the yaml desriptive file // don't fix it is an edge case (because some image are relative to the meta file some to the actual file and it most cases both are inside source)
        const temp = new URL(source, `https://example.com/`);
        const parsed = path.parse(temp.pathname);
        const a = path.resolve(path.join(post.inputFolder, imageDirectory));
        const b = path.resolve(path.join(post.inputFolder, videoDirectory));
        const c = path.resolve(path.join(post.inputFolder, audioDirectory));
        
        if (!source.startsWith(`.`) ||
            absoluteSource.startsWith(a) ||
            absoluteSource.startsWith(b) ||
            absoluteSource.startsWith(c)
        ) {
            post?.medias?.push(source);
        } else {
            // todo deduplicate
            const parsed = path.parse(absoluteSource);
            const mime2 = getMimeFromPath(parsed.base);
            const extension = getExtensionFromMime(mime2);
            const newName = `${slugify(parsed.base)}-${slugify(post.base)}-${i[0]}-${sep}.${extension}`;
            const parsedNewName = path.parse(newName);
            let newPath = `${post.outputFolder}${imageDirectory}${newName}`;
            let newSource = `../${imageDirectory}${newName}`;
            
            if (mime2.startsWith(`audio`)) {
                newPath = `${post.outputFolder}${audioDirectory}${newName}`;
                newSource = `../${audioDirectory}${newName}`;
            }
            if (mime2.startsWith(`video`)) {
                newPath = `${post.outputFolder}${videoDirectory}${newName}`;
                newSource = `../${videoDirectory}${newName}`;
            }
            if (!post.isTranslation) { // todo is this correct ?
                post.medias.push({
                    originalLocal: absoluteSource,
                    needsToBePlacedHere: newPath,
                });
            }
            return {source: newSource, parsed: parsedNewName, newName};
        }
        return {source, parsed};
    }
    let temp;
    if (isRelativeUrl(source) /* && post.isRemote*/) {
        temp = new URL(source, post.src);                    
    } else /*if absoluteUrl*/{
        temp = new URL(source);
    }
    const parsed = path.parse(temp.pathname);
    const mime2 = getMimeFromPath(parsed.base);
    const extension = getExtensionFromMime(mime2);
    const newName = `${slugify(post.base)}-${i[0]}-${sep}.${extension}`;
    const parsedNewName = path.parse(newName);
    let newPath = `${post.outputFolder}${imageDirectory}${newName}`;
    let newSource = `../${imageDirectory}${newName}`;
    
    if (mime2.startsWith(`audio`)) {
        newPath = `${post.outputFolder}${audioDirectory}${newName}`;
        newSource = `../${audioDirectory}${newName}`;
    }
    if (mime2.startsWith(`video`)) {
        newPath = `${post.outputFolder}${videoDirectory}${newName}`;
        newSource = `../${videoDirectory}${newName}`;
    }
    if (!post.isTranslation) { // todo is this correct ?
        post.medias.push({
            original: temp,
            needsToBePlacedHere: newPath,
        });
    }
    
        
    return {source: newSource, parsed: parsedNewName, newName};
};};

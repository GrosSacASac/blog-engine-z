export { normalizeDate, niceDateString };


const normalizeDate = function (anyDate) {
    if (typeof anyDate === `object` && anyDate.constructor === Date) {
        return anyDate.getTime();
    }
    if (typeof anyDate === `string`) {
        if (anyDate.includes(`/`)) {
            const asDate = new Date(anyDate);
            return normalizeDate(asDate);
        }
        return Number(anyDate);
    }
    return anyDate;
};


const niceDateString = function (date, lang) {
    // todo When formatting large numbers of dates, it is better to create an Intl.DateTimeFormat object and use the function provided by its format property.
    return new Date(date).toLocaleDateString(lang);
};

export { postFromFileDetails, processExternalPost, supportedFormats, getDetailsFromPost, initialPost, processPostAs, getPostDefault };

import fs from "node:fs";
import path from "node:path";
import stream from "node:stream";
import fsPromises from "node:fs/promises";
import {
    httpCodeFromText,
} from "http-code-from-text";
import { assignSelected } from "utilsac";
import { fetchWithCache } from "../defaultSettings/fetchCache.js";
import { getExtensionFromMime } from "./safe-mime.js";
import { isLocalUrl } from "./urls.js";
import { processPostAsHTML, formatHTML } from "./processors/html.js";
import { processPostAsTXT, formatTXT } from "./processors/txt.js";
import { processPostAsMD, formatMD } from "./processors/md.js";
import { processPostAsMulti, formatMulti } from "./processors/multi.js";

const supportedFormats = [
    ...formatMulti,
    ...formatHTML,
    ...formatTXT,
    ...formatMD,
];

// things that should be assignable by meta file
const assignMeta = assignSelected.bind(undefined, Object.keys({
    title: ``,
    coverImage: `post.jpg`,
    ready: true,
    indexed: true,
    listed: true,
    addSecret: ``,
    creationDate: 0,
    modifiedDate: 0,
    author: ``,
    description: ``,
    categories: [],
    tags: [],
    translations: [],
    js: [],
    lang: ``,
    editLink: undefined,
    canonicalUrl: undefined,
    commentsDisabled: true,
    commentsEnabled: false,
}));

// may return a promise
const postFromFileDetails = function (fileDetails, metas, commonOptions) {
    const { extension, fullPath } = fileDetails;
    if (!supportedFormats.includes(extension) || fileDetails.resolvedPath) {
        return;
    }

    const post = initialPost(fileDetails, commonOptions);
    decorateWithExternalMetaFileIfPresent(post, metas);

    post.getContentPromise = function () {
        return fsPromises.readFile(fullPath, `utf-8`);
    };
    post.getContentStream = function () {
        return fs.createReadStream(fullPath, `utf-8`);
    };
    return processPostAs(extension, post, commonOptions);
};

// may return a promise
const processPostAs = (extension, post, commonOptions) => {
    if (!supportedFormats.includes(extension)) {
        return;
    }

    if (formatHTML.includes(extension)) {
        return processPostAsHTML(post);
    }

    if (formatMD.includes(extension)) {
        return processPostAsMD(post);
    }

    if (formatTXT.includes(extension)) {
        return processPostAsTXT(post);
    }

    if (formatMulti.includes(extension)) {
        return processPostAsMulti(post, commonOptions); // promise
    }
};


const processExternalPost = function (post) {
    const { src } = post;
    post.isRemote = true;
    post.canonicalUrl = post.canonicalUrl ?? src;
    post.getHtmlBody = function () {
        let contentType = post[`Content-Type`];
        return fetchWithCache(src).then(response => {
            if (response.ok || response.status === Number(httpCodeFromText[`Not Modified`])) {
                contentType = contentType || response.headers.get(`Content-Type`);
                return response.text();
            }
            throw `failed to fetch ${src} because ${response.status} ${response.statusText} `;
        }).then(text => {
            let extension;
            if (contentType) {
                extension = getExtensionFromMime(contentType);
            } else {
                // guess with path
                // slice(1) to remove the dot from extension
                extension = path.parse(src).ext.slice(1);
            }
            post.getContentPromise = () => {
                return Promise.resolve(text);
            };
            post.getContentStream = () => {
                return stream.Readable.from(text);
            };
            return processPostAs(extension, post).getHtmlBody();
        }).catch(error => {
            console.warn(`could not fetch ${src}`, error);
            return `request failed`;
        });
    };
    return post;
};



const decorateWithExternalMetaFileIfPresent = function (post, metas) {
    const metaExtension = metas.find(function (otherPost) {
        return otherPost?.base2 === post.base2 || otherPost?.resolvedPath === path.resolve(post.fullPath);
    });
    
    post.externalMeta = metaExtension;
    if (!post.externalMeta) {
        return post;
    }

    return assignMeta(post, metaExtension);
};

const initialPost = function (fileDetails, commonOptions) {
    const {defaultAuthor, defaultLang} = commonOptions;
    return {
        src: `defined`,
        author: defaultAuthor,
        lang: defaultLang,
        inputFolder: commonOptions.inputFolder,
        outputFolder: commonOptions.outputFolder,
        ...fileDetails,
    };
};

const getPostDefault = function (post) {
    const { fullPath } = post;
    let stats;
    if (isLocalUrl(fullPath)) {
        stats = fs.statSync(fullPath);
    } else {
        stats = {};
    }
    return {
        medias: [],
        slides: [],
        src: `defined`,
        coverImage: `post.jpg`,
        isTranslation: false,
        ready: true,
        indexed: true,
        listed: true,
        addSecret: ``,
        externalMeta: undefined,
        creationDate: stats.birthtime,
        modifiedDate: stats.mtime,
        author: ``,
        description: ``,
        next: undefined,
        previous: undefined,
        categories: [],
        tags: [],
        generatedTags: [],
        finalTags: [],
        words: {},
        sortedWords: [],
        translations: [],
        js: [],
        lang: ``,
        editLink: undefined,
        isRemote: false,
        canonicalUrl: undefined,
    };
};



const getDetailsFromPost = function (fullPath) {
    const parsedPath = path.parse(fullPath);
    const base = parsedPath.name;
    const extension = path.extname(fullPath).substr(1);
    const title = parsedPath.name;
    if (!supportedFormats.includes(extension) && extension !== ``) {
        console.warn(`File format from ${fullPath} is ${extension} extension and not recognized.
        Use one of ${supportedFormats.join(`, `)} instead.`);
    }

    const resolvedDir = path.posix.resolve(parsedPath.dir);
    let folderCategory;
    let base2 = base;
    if (resolvedDir.includes(`source/`)) { // can do better
        const temp = resolvedDir.substring(
            resolvedDir.indexOf(`source`) + `source`.length + 1,
        );
        if (temp) {
            folderCategory = temp.replaceAll(`/`, `-`);
            base2 = `${folderCategory}_${base}`;
        }
    }
    return { title, extension, base , dir: parsedPath.dir, fullPath, folderCategory, base2};
};

export { generateSearchCode };

import path from "node:path";
import url from "node:url";
import fsPromises from "node:fs/promises";
import {rollup} from "rollup/dist/es/rollup.js";
import { terser } from "rollup-plugin-terser";
import { searchCodeTemplate } from "../js/search.js";


const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


let codeWithImports;
let lunrCode;

const SEARCH = `SEARCH`;
const getResolveUrl = (importPath, URL = `URL`) => {
    return `new ${URL}(${importPath}).href`;
};
const relativeUrlMechanisms = {
    amd: relativePath => {
        if (relativePath[0] !== `.`)
            {relativePath = `./${  relativePath}`;}
        return getResolveUrl(`require.toUrl('${relativePath}'), document.baseURI`);
    },
    cjs: relativePath => {
        return `(typeof document === 'undefined' ? ${getResolveUrl(`'file:' + __dirname + '/${relativePath}'`, `(require('u' + 'rl').URL)`)} : ${`error`})`;},
    es: relativePath => {return getResolveUrl(`'${relativePath}', import.meta.url`);},
    iife: () => {return `error`;},
    system: relativePath => {return getResolveUrl(`'${relativePath}', module.meta.url`);},
    umd: relativePath => {return `(typeof document === 'undefined' ? ${getResolveUrl(`'file:' + __dirname + '/${relativePath}'`, `(require('u' + 'rl').URL)`)} : ${`error`})`;},
};

const customPlugin = {
    name: `customPlugin`,
    resolveFileUrl({ relativePath, format }) {
        // todo is this ever executed ?
        return relativeUrlMechanisms[format](relativePath);
    },
    resolveId(importee/*, importer*/) {
        // return false to not continue
        // return a string to let load do the rest
        // console.log(importee, importer)
        return importee;
    },
    load: function (id) {
        // console.log(id)
        
        if (id === SEARCH) {
            return Promise.resolve(codeWithImports);
        } if (id === `lunr`) {
            return Promise.resolve(lunrCode);
        }
        throw new Error(`not import`);
    },
};
const inputOptions = {
    input: SEARCH,
    treeshake: {
        moduleSideEffect: `no-external`,
        propertyReadSideEffects: false, // assume reading properties has no side effect
    },
    plugins: [customPlugin, terser()],
};

const outputOptions = {
    format: `es`,
    name: null,
    file: null,
};

const generateSearchCode = async (posts, options) => {
    codeWithImports = searchCodeTemplate(posts, options);
    try {
        lunrCode = await fsPromises.readFile(`${__dirname}/../node_modules/blog-engine-sac/node_modules/lunr/lunr.js`, `utf-8`);
    } catch (_) {
        lunrCode = await fsPromises.readFile(`${__dirname}/../../blog-engine-sac/node_modules/lunr/lunr.js`, `utf-8`);
    }
    lunrCode = lunrCode.replace(`}(this, function () {`, `}(window, function () {`);
    lunrCode = `${lunrCode}; export default lunr`;
    const bundle = await rollup(inputOptions);

    //console.log(bundle.imports); // an array of external dependencies
    const output = await bundle.generate(outputOptions);
    // console.log(output)
    return output.output[0].code;
};

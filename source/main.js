

import { buildSite } from "./buildSite.js";
import { parseCli } from "./parseCli.js";


const cliOptions = parseCli();





buildSite(cliOptions).catch(error => {
    if (error.code === `ENOENT`) {
        console.error(`source not found in ${cliOptions.inputFolder}`);
        return;
    }
    throw error;
});

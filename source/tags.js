export { generateTags, wordCount, setFinalTags };


const maxSeek = 0.9;
const minimumRarity = 0.6;
const minimumOccurences = 1;
const maxTags = 10;

const generateTags = function (posts, allWords) {
    /* we have counted all the words
    tags are the most common words in each posts
    excluded are the most frequent word in all post
    to avoid commond words (the, in, all, etc)
    
    allWords.totalCount and allWords.sortedWords.length are not the same
    allWords.sortedWords.length is equal to the unique amount of words
    
    allWords.sortedWords is sorted from most common to rarest
    post.sortedWords is reversed() */
    const minimumRarityIndex = Math.floor(allWords.sortedWords.length * minimumRarity);
    posts.forEach(function (post) {
        post.sortedWords.reverse();
        const minimumCommonIndex = Math.floor(post.sortedWords.length * maxSeek);
        for (let i = minimumCommonIndex; i < post.sortedWords.length; i += 1) {
            const word = post.sortedWords[i][0];
            const globalPosition = allWords.sortedWords.findIndex(function ([wordI/*, count*/]) {
                return wordI === word;
            });
            if ((globalPosition < minimumRarityIndex) &&
                (post.generatedTags.length < maxTags) &&
                (post.sortedWords[i][0] >= minimumOccurences)) {
                post.generatedTags.push(word);
            }
        }
    });
};

const setFinalTags = function (posts) {
    posts.forEach(function (post) {
        post.finalTags = Array.from(new Set([
            ...post.tags,
            ...post.generatedTags,
            ...post.categories,
        ]));
        post.searchTags = Array.from(new Set([
            ...post.finalTags,
            ...post.title.split(` `).filter(Boolean),
        ]));
    });
};

const wordCount = function (posts) {
    const allWords = {
        totalCount: 0,
        words: {},
    };
    posts.forEach(function (post) {
        // todo raw does not exist anymore
        if (!post.raw) {
            post.raw = ``;  
            post.totalCount = 0;
            return;
        }
        const words = post.raw.split(` `);
        const { length } = words;
        allWords.totalCount += length;
        post.totalCount = length;
        words.forEach(function (word) {
            if (Object.hasOwn(allWords.words, word)) {
                allWords.words[word] += 1;
            } else {
                allWords.words[word] = 1;
            }
            if (Object.hasOwn(post.words, word)) {
                post.words[word] += 1;
            } else {
                post.words[word] = 1;
            }
        });
        sortWords(post);
    });
    sortWords(allWords);
    return allWords;
};

const sortWords = function (wordContainer) {
    wordContainer.sortedWords = Object.entries(wordContainer.words);
    wordContainer.sortedWords.sort(function (wordA, wordB) {
        return wordA[1] - wordB[1];
    });
};

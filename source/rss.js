import { Feed } from "feed";
import * as urls from "../defaultSettings/urls.js";
import { postStartName } from "../html/links.js";
import {
    imageDirectory,
} from "../defaultSettings/paths.js";
import * as resolutions from "../defaultSettings/resolutions.js";

export {feedString};

const feedString = function(options, posts) {
    const image = `${options.url}${imageDirectory}${resolutions.small}-${options.homePageImage}`;
    const favicon = `https://letz.social/favicon.ico`;
    const feed = new Feed({
        title: options.mainTitle,
        description: options.subTitle,
        id: options.url,
        link: options.url,
        language: options.defaultLang, // optional, used only in RSS 2.0, possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
        image, // full url
        favicon,
        copyright: options.defaultLicense,
        // updated: new Date(2013, 6, 14), // optional, default = today
        generator: "blog-engine-sac", // optional, default = 'Feed for Node.js'
        feedLinks: {
            json: `${options.url}feed.json`,
            atom: `${options.url}feed.atom`
        },
        author: {
            name: options.defaultAuthor,
            // email: "johndoe@example.com",
            // link: "https://example.com/johndoe"
        }
    });
    posts.forEach(post => {
        
        const selfUrl = `${options.url}${urls.posts}${postStartName(post)}${options.extensionLinks}`;
        const date = new Date();
        date.setTime(post.creationDate);
        let image = favicon;
        if (post.coverImage) {
            image = `${options.url}${imageDirectory}${resolutions.small}-${post.coverImage}`
        }
        feed.addItem({
            title: post.title,
            id: selfUrl,
            link: selfUrl,
            description: post.description,
            content: "",
            author: [
            {
                name: post.author,
                // email: "janedoe@example.com",
                // link: "https://example.com/janedoe"
            },
            ],
            contributor: [
            // {
            //     name: "Shawn Kemp",
            //     email: "shawnkemp@example.com",
            //     link: "https://example.com/shawnkemp"
            // },
            // {
            //     name: "Reggie Miller",
            //     email: "reggiemiller@example.com",
            //     link: "https://example.com/reggiemiller"
            // }
            ],
            date: date,
            image,
        });
    });
    const temp = [];
    options.categories.forEach(({tag}) => {
        if (!temp.includes(tag)) {
            feed.addCategory(tag);
            temp.push(tag);
        }
    });

    // feed.addContributor({
    // // name: "Johan Cruyff",
    // // email: "johancruyff@example.com",
    // // link: "https://example.com/johancruyff"
    // });
    return {
        rss2: feed.rss2(),
        atom1: feed.atom1(),
        feedJson1: feed.json1(),
    };
};

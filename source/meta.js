export { processMetaFile };

import path from "node:path";
import { load, supportedExtensions } from '@grossacasacs/left-phalange';
import isRelativeUrl from "is-relative-url";


const supportedFormats = supportedExtensions;
const processMetaFile = async (detailedSource) => {
    // hanldes files without src (src is another file with same base name)
    const { extension, fullPath, base, base2 } = detailedSource;
    if (!supportedFormats.includes(extension)) {
        return;
    }

    let postObject;
    try {
        postObject = load(fullPath);
    } catch (error) {
        console.error(`could not load ${fullPath} as one of ${supportedExtensions}`);
        console.error(error);
        return;
    }
    const { src } = postObject;

    if (!src) {
        return Object.assign(postObject, {base, base2});
    }
    
    if (isRelativeUrl(src)) {
        delete postObject.src;
        const resolvedPath = path.resolve(detailedSource.dir, src);
        postObject.resolvedPath = resolvedPath;
        detailedSource.resolvedPath = resolvedPath;//to make it filtered out
        return postObject;
    }
    
    return false;
};

export { isLocalUrl };
// import { URL } from "node:url";


const isLocalUrl = (x) => {
    let parsed;
    try {
        parsed = new URL(x);
    } catch {
        // throws if origin is missing
        return Boolean(x);
    }
    return !parsed.host;
};
